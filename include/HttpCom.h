#pragma once
#if defined( _WIN32) && !defined(__MINGW32__)
#pragma warning(disable:4251)
#endif

#include "HttpMsg.h"

#include "Sock.h"
#if !defined(__MINGW32__)
#define HAVE_SSL
#endif

#ifdef HAVE_SSL
#include "SslSock.h"
#endif

#include "defs.h"

#include <string>

using namespace std;

// 定数定義
const unsigned short HttpCom_usHttpPort=80;
const unsigned short HttpCom_usHttpsPort=443;

// 返り値
const int HTTPCOM_RETURN_OK = 0;
const int HTTPCOM_ALREADY_OPEN = 1;
const int HTTPCOM_NOT_CONNECTED = 2;
const int HTTPCOM_MSGRECV_INCOMPLETE = 3;
const int HTTPCOM_OPTION_UNSUPPORTED = 100;
const int HTTPCOM_HTTPMSG_INVALID = 800;
const int HTTPCOM_SHORTAGE_REQ_HEADER = 801;	// ヘッダが不足
const int HTTPCOM_HEADER_NOT_FOUND = 802;		// 受信ヘッダが見つからない
const int HTTPCOM_INVALID_URL = 803;			// URLが不正

// HTTP Response Code
const unsigned short HTTPCOM_RESP_OK=200;

// 構造体定義
typedef struct{

}HTTP_Header;

/** HTTP通信クラス
 *
 */
class DLL_EXPORT CHttpCom
{
public:
	// Protocol
	enum Protocol{
		HTTP=0,
#ifdef HAVE_SSL
		HTTPS,
#endif
	};
	/** Method */
	enum HttpMethod{
		GET = 0,
		POST,
	};
	// HTTP Header
	enum Header{
		HEADER_CONTENT_TYPE = 0,
		HEADER_HOST_NAME,
		HEADER_LOCATION,
		HEADER_CONTENT_LEN,
		HEADER_PRESET_NUM,
	};
	// Option ITEM
	enum Option{
//		OPT_HTTP_VER = 0,
//		OPT_HOSTNAME_SEND,
		OPT_COOKIE_SEND,
	};

	CHttpCom(const CHttpMsg::HTTPVer);
	~CHttpCom(void);

	int SetOption(const Option item, const string *val);
	int SetOption(const Option item, bool flag);

	int SetTimeout(const unsigned long msec);

	int ConnectServer(const string * server, const unsigned short port = 80, const Protocol proto = HTTP);
	int Close();

	// 送信側の準備
	int SendMsgInit();
	int SetHttpHeader(const Header item, const string *val);
	int SetBody(const string *body);
	int SendRequest(const string * page, HttpMethod method = GET);
	int DumpRequestMsg(string *msg);

	// 受信
	int GetResponse(unsigned short *code);
	int GetHttpHeader(const Header item, string *val);
	int GetResBody(string * html);
	int DumpResponseMsg(string *msg);

	// URLのParse
	static int ParseUrlString(const string * url, Protocol *proto, string * server, unsigned short *port, string *page);

protected:
	// Method
//	int mHttpRecv(string & dat);
	string mGetRequestString(const string *page, HttpMethod method);
	void mSetDefaultOption(void);

	void mInit();
	int mRecvByte(char & ch);
	int mRecvLine(string & line);
	int mRecvBody(const size_t size, string & body);
	
	bool mIsActive(void);

	// Property
	Protocol m_proto;	// 接続プロトコル
	CSock *m_pSock;		
#ifdef HAVE_SSL
	CSslSock *m_pSsl;
#endif

	// data
	CHttpMsg m_httpSendMsg;
	CHttpMsg m_httpRecvMsg;

	// Option
//	string m_httpVer;					// HTTPバージョン文字列
	CHttpMsg::HTTPVer m_httpVer;
//	bool m_fSendHost;					// hostnameを送信するか
	bool m_fCookieCont;					// 受信したCookieを続けて送信するか

	// Message
	string m_lastSendMsg;
	string m_lastRecvMsg;

	string *m_cSHeader;

};
