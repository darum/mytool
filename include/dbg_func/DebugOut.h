#include <string>
#ifdef _WIN32
#include "tchar.h"

#define _MY_TCHAR TCHAR

#ifdef _DEBUG
#   define MyOutputDebugString( str, ... ) \
      { \
        TCHAR c[256]; \
		_stprintf_s( c, 256, str, __VA_ARGS__ ); \
        OutputDebugString( c ); \
      }
#else
#   define MyOutputDebugString( str, ... )

#endif
#endif

#if 0
class CFuncTrace
{
public:
	CFuncTrace(const TCHAR *str)
	{
		_tcscpy_s(m_name, 256, str);
		MyOutputDebugString(_T("IN: %s\n"), m_name);
	}

	~CFuncTrace()
	{
		MyOutputDebugString(_T("OUT %s\n"), m_name);
	}

private:
	_MY_TCHAR m_name[256];
};
#endif
