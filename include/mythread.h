#ifdef _WIN32
#pragma warning(disable : 4819) 
#endif

#ifndef __MY_THREAD_H__
#define __MY_THREAD_H__

#include "defs.h"
#ifdef _WIN32
#include <windows.h>
#include <process.h>
#define THREAD_RET DWORD
#define THREAD_WAIT_INFNITE INFINITE
#else
#include <pthread.h>
#define THREAD_RET int
#define THREAD_TYPE unsigned 
#define THREAD_WAIT_INFNITE 0
#endif

namespace mythread
{
  /** スレッド Interface Class
   * スレッドのインタフェース
   * @version 0.10
   * @date 2008/10/01
   */
  class DLL_EXPORT thread
  {
  private:
    static THREAD_RET start_routine(void *);
  
  public:
    thread();
    virtual ~thread();
    virtual int start();
    virtual int join(THREAD_RET *ret = NULL, int wait = 0);
	virtual int stop(THREAD_RET *ret = NULL, int wait = 0);
#ifdef _WIN32
	virtual bool get_run_state();
#endif
  
  protected:
    virtual THREAD_RET run();
	bool m_run;
#ifdef _WIN32
	DWORD m_threadid;
#else
    pthread_t m_threadid;
#endif
  };


};
#endif

