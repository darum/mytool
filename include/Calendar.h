#pragma once
#if defined(_MSC_VER)
#pragma warning(disable:4251)		// vector
#endif

#ifndef _CALENDAR_H_
#define _CALENDAR_H_

#include <stdio.h>
#include <string>
#include <vector>

#include "defs.h"

typedef long CALDATE;

using namespace std;

/** 休日情報クラス
 * カレンダクラスの休日情報用クラス
 * @version 0.10
 * @date 2009/02/24
 */
class DLL_EXPORT CHoliday
{
public:
	CHoliday();
	bool AddHoliday( CALDATE hol );
	bool DelHoliday( CALDATE hol );
	bool Find( CALDATE  );

protected:
	int m_Find(const CALDATE target);

private:
	vector<CALDATE> m_tbl;
};

/** カレンダークラス
 * 休日情報を保持し、カレンダー日／営業日での各計算を行います。
 * @note 保持できる日数は、2^31 までで、それ以上登録した場合の処理は保証されません。
 * @version 0.10
 * @date 2008/12/21
 * 
 */
class DLL_EXPORT CCalendar
{
public:
	CCalendar(void);
//	CCalendar( const CALDATE & date );
	~CCalendar(void);

	// 設定
	static CALDATE MakeCalDate( unsigned int year, unsigned int month, unsigned int day );
//	bool SetDate( unsigned int year, unsigned int month, unsigned int day );
//	void AddDays( unsigned long add );
//	void SubDays( unsigned long sub );

//	bool AddHoliday( CCalendar & hol, bool adjust = true );
	bool AddHoliday( CALDATE hol, bool adjust = true );
	unsigned int AddHolidayMon( unsigned int month, unsigned int week, unsigned int sYear, unsigned int eYear=0 );

	
	// 日付取得
	static void GetAddDays( CALDATE &, unsigned long add );
	static void GetSubDays( CALDATE &, unsigned long sub );

	void GetBizAddDays(CALDATE &, unsigned long add);
	void GetBizSubDays(CALDATE &, unsigned long sub);

	// 計算
	long GetBizDiff( const CALDATE base, const CALDATE diff );
	long GetDiff( const CALDATE base, const CALDATE diff );

	// 判断
	bool IsHoliday( CALDATE date );

	// 属性取得
//	unsigned int GetYear();
//	unsigned int GetMonth();
//	unsigned int GetDay();
	static unsigned short int GetWeekDay( CALDATE date );
	static string toString(CALDATE date);

private:
//	CALDATE mDateVal;
	CHoliday mpHoliday;


	/** ライブラリの開始日 */
	static const CALDATE START = 19480101;
	/** ライブラリの終了日 */
	static const CALDATE END = 20501231;

protected:
	// 取得
	static CALDATE	m_GetAddDay( const CALDATE & );
	static CALDATE	m_GetSubDay( const CALDATE & );
	static unsigned short int m_GetWeekDay( const CALDATE & );

	// 判定
	static bool		m_IsValidDate( const CALDATE & );
	static bool		m_IsHoliday( const CALDATE & date, CHoliday & hol );
};

#endif
