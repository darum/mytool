#pragma once
#include "Sock.h"

#include "defs.h"

#include <openssl/crypto.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>

// Return Code (ERROR CODE)
const short int SSLSOCK_ORIGINAL_ERR = 0x5000;
const short int SSLSOCK_INIT_ERR = (1 | SSLSOCK_ORIGINAL_ERR);
const short int SSLSOCK_CON_ERR = (2 | SSLSOCK_ORIGINAL_ERR);
const short int SSLSOCK_ALREADY_INIT = (3 | SSLSOCK_ORIGINAL_ERR);

// Connection Status
#define SOCK_STAT_SSLCLOSE 10

/** SSL Socket Connection Class
 * @version 0.10
 * @date yy/mm/dd
 */
class DLL_EXPORT CSslSock :
	public CSock
{
public:
	CSslSock(void);
	~CSslSock(void);

	// Override
	bool OpenCli( int family, int type, const char *addr, const unsigned short port );
	bool Close();

	bool Recv( void *buf, size_t count, int flags=0, ssize_t *szRecv=NULL, struct timeval *timeout = NULL );
	bool Send( const void *buf, size_t count, int flags=0, size_t *szSend=NULL );

	char *GetErrorMessage(char *buf, size_t len);

protected:
	int m_sslOpen();
	int m_sslClose();
	int m_close();

	// not implemented (non-overrided)
	bool OpenSvr( int family, int type, unsigned short port, const char *addr=(char *)NULL );
	bool Listen();
	CSock *Accept();
	bool SetFd( const SOCK_DESC );

	bool GetPeerAddress( struct sockaddr & );

protected:
	SSL *m_ssl;			// SSL Pointer
	SSL_CTX *m_ctx;		// 

};
