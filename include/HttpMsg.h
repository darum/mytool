#pragma once

#include <string>
#include <map>
#include <vector>

using namespace std;

struct HttpMsgInfo {
	string httpVer;
	unsigned short status;
	string strStatus;

};

struct CookieInfo {
	string data;		// Cookieデータ
	time_t expire;		// 有効期限。0で無期限
	string path;		// 有効なパス
	string domain;		// 有効なドメイン
	bool isSecure;		// secure ON/OFF
};

/** HTTPメッセージ クラス
 *
 */
class CHttpMsg
{
public:
	enum HTTPVer{
		HTTP_VER_1_0 = 0,
		HTTP_VER_1_1,
		HTTP_VER_NUM
	};

	CHttpMsg(void);
	~CHttpMsg(void);

	// Set Method
	int SetHeader(const string item, const string value);
	int SetCookie(const string item, const string value);
	int SetBody(const string *body);
	int CheckHeader(CHttpMsg::HTTPVer & ver);
	int GetHttpMsg(string *msg, bool secure = false);

	// Clear Method
	int ClearAll();
	int ClearHeader();
	int ClearBody();

	// Get Method
//	int ParseMessage(const string *msg);
	int ParseStatusLine(const string *line);
	int ParseHeaderLine(const string *msg);
	int GetStatusInfo(HttpMsgInfo *pInfo);
	int GetHeader(const string item, string *val);
	int GetBody(string *msg);
	int GetContentLen(void);

protected:
	// Method
	int mParseCookieData(string & cookie);
	int mSetCookie(string *msg);

	// Property
	// Header（送受信とも）
	map<string, string> m_header;

	// Cookie（送受信とも）
	vector<CookieInfo> m_cookie;

	// Body（受信データ）
	string m_body;

	// 受信メッセージの状態
	struct HttpMsgInfo m_replyInfo;

};
