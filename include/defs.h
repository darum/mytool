#ifndef __DEFS_H__
#define __DEFS_H__
#if defined(_WIN32) && !defined(__GNUC__)	// MinGW除く

#ifdef DLL_EXPORT_DO		//これは StdAfx.h で定義されています。
   #define DLL_EXPORT			__declspec(dllexport)
#else
   #define DLL_EXPORT			__declspec(dllimport)
#endif

#else
#define DLL_EXPORT
#endif	// _WIN32
#endif	//__DEFS_H__
