#ifndef _C_LOG_H_
#define _C_LOG_H_

#include <string>

#include "defs.h"

using namespace std;

namespace myLog
{

#define LOG_PROC_OK 0

#define CLOG_MAX_LEVEL 4
#define CLOG_MAX_szLVStr 5

	/**
	 * @brief Log Writer
	 * 
	 */
	class DLL_EXPORT CLog
	{
	public:
		/**
		 * @brief Constructor
		 * @param file File name
		 */
		CLog( const string & file );
		~CLog();
		
		int Write( unsigned char level, const char *format, ... );

		int DebugWrite( const char *format, ... );
	     
		enum CLOG_LEVEL{
			LV_ERROR = 0,
			LV_WARNING,
			LV_INFO,
			LV_DEBUG
		};

		
	protected:
		/** File Name
		 *
		 */
		 string m_fname;
		/** Output Level
		 *
		 */
		char m_outLv;

	};

#ifdef _DEBUG
#define LOG_STDOUT "-"
#endif

};

#endif
