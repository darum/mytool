/**
 *  CSock: Socket class for C++
 */
#ifndef __CSOCK_H__
#define __CSOCK_H__

#include <stddef.h>
#include <string.h>
#include <stdlib.h>

#ifdef __MINGW32__
#define WINVER 0x0501		// WindowsXP
#endif

#ifdef _WIN32
#include <winsock2.h>
#include <BaseTsd.h>
#ifndef __MINGW32__
typedef SSIZE_T ssize_t;
#endif

#include <Ws2tcpip.h>
#else // _WIN32
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#endif
#ifdef __FreeBSD__
#include "netinet/in.h"

#define EAI_ADDRFAMILY 0
#endif

#include <string>

#include "defs.h"

using namespace std;

// Connection Status
#define SOCK_STAT_CLOSE 0
#define SOCK_STAT_OPEN 1
#define SOCK_STAT_LISTEN 2
#define SOCK_STAT_CONNECT 3

// Original Error
const short int SOCK_ORIGINAL_ERR = 0x6000;
const short int SOCK_NO_ERROR = 0;
const short int SOCK_CONNECT_EXIST = (1 | SOCK_ORIGINAL_ERR);
const short int SOCK_MEMORY_ERROR = (2 | SOCK_ORIGINAL_ERR);
const short int SOCK_CONNECT_NOOPEN = (3 | SOCK_ORIGINAL_ERR);
const short int SOCK_PARAM_ERROR = (4 | SOCK_ORIGINAL_ERR);
const short int SOCK_CREATE_ERROR = (5 | SOCK_ORIGINAL_ERR);
const short int SOCK_CLOSE_ERROR = (6 | SOCK_ORIGINAL_ERR);
const short int SOCK_INTERNAL_ERROR = (0xFFF | SOCK_ORIGINAL_ERR);
const short int SOCK_RECV_TIMEOUT = (7 | SOCK_ORIGINAL_ERR);
const short int SOCK_ALREADY_CLOSE = (8 | SOCK_ORIGINAL_ERR);
const short int SOCK_ADDR_ERROR = (9 | SOCK_ORIGINAL_ERR);

// Can't use "MSG_DONTWAIT" in this class (which is used at receiving message)
#ifdef MSG_DONTWAIT
#undef MSG_DONTWAIT
#endif
// Define the Macro if MSG_NOSIGNAL is not defined in System
#ifndef MSG_NOSIGNAL
#define MSG_NOSIGNAL 0	// 
#endif

#ifdef _WIN32
const WORD winsockVer = ((2 << 8) + 2 );	//Winsock 2.2
#endif

#ifdef _MSC_VER
#define SOCK_DESC SOCKET
#define MYSOCK_ERR SOCKET_ERROR
#else
#define SOCK_DESC int
#define MYSOCK_ERR (-1)
#endif


/**
 * @brief Socket Connection Class
 * Socket Connection Management Class
 * @version 0.50
 * @date 2008/09/28
 */
	class DLL_EXPORT CSock
	{
		public:
			CSock();
			~CSock();

			bool OpenSvr( int family, int type, unsigned short port, const char *addr=(char *)NULL );
			bool Listen();
			CSock *Accept();
			bool OpenCli( int family, int type, const char *addr, const unsigned short port );
			bool Close();

			bool Recv( void *buf, size_t count, int flags=0, ssize_t *szRecv=NULL, struct timeval *timeout = NULL );
			bool Send( const void *buf, size_t count, int flags=0, size_t *szSend=NULL );

			bool SetFd( const SOCK_DESC );

			short int GetLastError();

			// Set Peer Descriptor at Server
			bool GetPeerAddress( struct sockaddr & );
			unsigned char GetConStat();

		private:
			// Properties
			SOCK_DESC m_fd;
			struct sockaddr *m_pAddr;	// Peer Address

		protected:
			unsigned char m_conStat;	// Connected Status
			short int m_error;	// Error Status

		protected:
			bool m_socket( int family, int type );
			int m_close();

			bool mSetAddr( const struct sockaddr * );
			bool mGetAddr( struct sockaddr & );
			ssize_t m_read( void *buf, size_t count, int flags, struct timeval *timeout );
			ssize_t m_write( const void *buf, size_t count, int flags );

			SOCK_DESC m_getFd();
	};

#endif
