// File Format:
// [SECTION]
// item=value

#pragma once
#ifdef WIN32
#pragma warning(disable:4251)		// vector
#endif

#ifndef _CFG_LOAD_H_
#define _CFG_LOAD_H_

#include <string>
#include <fstream>
#include <map>

#include "defs.h"		// DLL

// Constant Value
#define BUF_SIZE 256

// Return Code
#define CFG_PROC_OK 0
#define CFG_ERROR_FOPEN -1
#define CFG_ERROR_FORMAT -2
#define CFG_ERROR_NOREC -3
#define CFG_ERROR_NOSECTION -4
#define CFG_ERROR_LENGTH		-5

#define CFG_NOTFOUND_SECTION -11
#define CFG_NOTFOUND_ITEM -12

// Name Space
using namespace std;

typedef map<string, string> sec_data;

/**
 * @brief Config File Load Class
 * Config Fileのロードを行います。
 * @version 0.90
 * @date 2008/12/13
 * 
 */
class DLL_EXPORT CCfgLoad
{
public:
	CCfgLoad(void);
	~CCfgLoad(void);

	int CfgOpen(const string & file);
	int CfgSave();

	int GetValue( const string &section, const string& item, int *ret );
	int GetValue( const string& section, const string& item, double *ret );
	int GetValue( const string& section, const string & item, long * ret );
	int GetHexValue( const string& section, const string & item, long * ret );

	int GetValue( const string& section, const string& item, string *ret );
	int GetValue( const string& section, const string& item, char *value, const size_t vLen );

	int SetValue( const string& section, const string& item, const double val);

	// Get Error
	unsigned int GetErrorLine();

	// Parameter Check
#ifndef _WIN32
	string GetCfgName();
#endif
	int GetCfgName(char *, size_t size);
	int GetSecCnt();
	int GetItemCnt( const string& item );

private:
	int mCfgFileRead(ifstream &fin);
	int mGetValueStr( const string& section, const string& item, string *ret );
	int mCfgFileWrite(ofstream &fout);
	int mSetValueStr(const string& section, const string &item, const string &val);

private:
	// Parameters
	string mCfgFile;				//! コンフィグファイル
	map <string, sec_data > mData;	//! 読みだしたデータ
	unsigned int mErrLine;			//! エラーがある行番号
};

#endif
