// MyTools_Debug.h : PROJECT_NAME アプリケーションのメイン ヘッダー ファイルです。
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CMyTools_DebugApp:
// このクラスの実装については、MyTools_Debug.cpp を参照してください。
//

class CMyTools_DebugApp : public CWinApp
{
public:
	CMyTools_DebugApp();

// オーバーライド
	public:
	virtual BOOL InitInstance();

// 実装

	DECLARE_MESSAGE_MAP()
};

extern CMyTools_DebugApp theApp;