// MyTools_DebugDlg.h : ヘッダー ファイル
//

#pragma once
#include "afxwin.h"


// CMyTools_DebugDlg ダイアログ
class CMyTools_DebugDlg : public CDialog
{
// コンストラクション
public:
	CMyTools_DebugDlg(CWnd* pParent = NULL);	// 標準コンストラクタ

// ダイアログ データ
	enum { IDD = IDD_MYTOOLS_DEBUG_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV サポート


// 実装
protected:
	HICON m_hIcon;

	// 生成された、メッセージ割り当て関数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedStart();
	afx_msg void OnOK();
	afx_msg void OnCancel();

private:
	CEdit mEdit;
protected:
	CButton mBtnStart;
	CButton mBtnExit;
};
