// MyTools_DebugDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MyTools_Debug.h"
#include "MyTools_DebugDlg.h"

#include "defs.h"
#include "testCalendar.h"
#include "testMyThread.h"
#include "testLog.h"
#include "testCfgLoad.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// アプリケーションのバージョン情報に使われる CAboutDlg ダイアログ

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// ダイアログ データ
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

// 実装
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMyTools_DebugDlg ダイアログ




CMyTools_DebugDlg::CMyTools_DebugDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMyTools_DebugDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMyTools_DebugDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, mEdit);
	DDX_Control(pDX, ID_START, mBtnStart);
	DDX_Control(pDX, IDOK, mBtnExit);
}

BEGIN_MESSAGE_MAP(CMyTools_DebugDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(ID_START, &CMyTools_DebugDlg::OnBnClickedStart)
END_MESSAGE_MAP()


// CMyTools_DebugDlg メッセージ ハンドラ

BOOL CMyTools_DebugDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// "バージョン情報..." メニューをシステム メニューに追加します。

	// IDM_ABOUTBOX は、システム コマンドの範囲内になければなりません。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// このダイアログのアイコンを設定します。アプリケーションのメイン ウィンドウがダイアログでない場合、
	//  Framework は、この設定を自動的に行います。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンの設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンの設定

	// TODO: 初期化をここに追加します。

	return TRUE;  // フォーカスをコントロールに設定した場合を除き、TRUE を返します。
}

void CMyTools_DebugDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// ダイアログに最小化ボタンを追加する場合、アイコンを描画するための
//  下のコードが必要です。ドキュメント/ビュー モデルを使う MFC アプリケーションの場合、
//  これは、Framework によって自動的に設定されます。

void CMyTools_DebugDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// クライアントの四角形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンの描画
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// ユーザーが最小化したウィンドウをドラッグしているときに表示するカーソルを取得するために、
//  システムがこの関数を呼び出します。
HCURSOR CMyTools_DebugDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMyTools_DebugDlg::OnBnClickedStart()
{
	// テスト結果を文字列ストリームに流し入れ、
	std::ostringstream stream;
	INT len;

	CString out = CString(_T("Test Execute...\r\n"));
	len = this->mEdit.GetWindowTextLength();
	this->mEdit.SetSel(len, len);
	this->mEdit.ReplaceSel(out);

	// Test Suite
	CPPUNIT_NS::TestSuite *all_test[] = {
		CCfgLoadTest::suite(),
		CCalendarTest::suite(), 
		CMyThreadTest::suite(),
		CLogTest::suite(),
		(CPPUNIT_NS::TestSuite *)NULL};
	CPPUNIT_NS::TestSuite *it;

	UINT p = 0;

	this->mBtnStart.EnableWindow(FALSE);

	it = all_test[p];
	while(it != NULL)
	{
		// イベント・マネージャとテスト・コントローラを生成する
		CPPUNIT_NS::TestResult controller;
		// テスト結果収集リスナをコントローラにアタッチする
		CPPUNIT_NS::TestResultCollector result;
		controller.addListener( &result );

		CPPUNIT_NS::Outputter *outputter = new CPPUNIT_NS::TextOutputter(&result, stream);

		// テストランナー
		CPPUNIT_NS::TextUi::TestRunner runner;
		runner.setOutputter(outputter);
		runner.addTest( it );

		stream.str("");
		stream << "**** Start   TEST **** (" << it->getName() << ")\n";
		runner.run( controller );
		outputter->write();
		stream << "**** Finish  TEST ****\n";

		std::string strOut = stream.str();
		unsigned long l;
		for( l = 0; l < strOut.size(); ++l )
		{
			if( strOut[l] == '\n' )
			{
				strOut.replace( l, 1, "\r\n" );
				l++;
			}
		}
		CString msg(strOut.c_str());
		// 出力
		len = this->mEdit.GetWindowTextLength();
		this->mEdit.SetSel(len, len);
		this->mEdit.ReplaceSel(msg);
		this->mEdit.ReplaceSel(_T("\r\n"));

		it = all_test[++p];
	}
	this->mBtnStart.EnableWindow(TRUE);

#if 0
	out = CString(_T("Finish.\r\n\r\n"));
	len = this->mEdit.GetWindowTextLength();
	this->mEdit.SetSel(len, len);
	this->mEdit.ReplaceSel(out);
#endif
}

void CMyTools_DebugDlg::OnOK() 
{
    CDialog::OnOK();
}

void CMyTools_DebugDlg::OnCancel() 
{
    CDialog::OnCancel();
}