// MySocket_DebugDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MySocket_Debug.h"
#include "MySocket_DebugDlg.h"

// Test Class
#include "defs.h"
#include "testSock.h"
#include "testSslSock.h"
#include "testHttpCom.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define _TEST_BUF_SIZE 1024

// Test Suite
CPPUNIT_NS::TestSuite *all_test[] = {
	CHttpComTest::suite(),
	CSslSockTest::suite(),
	CSockTest::suite(),
	(CPPUNIT_NS::TestSuite *)NULL
};

// アプリケーションのバージョン情報に使われる CAboutDlg ダイアログ

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// ダイアログ データ
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

// 実装
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMySocket_DebugDlg ダイアログ




CMySocket_DebugDlg::CMySocket_DebugDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMySocket_DebugDlg::IDD, pParent)
{
	AfxInitRichEdit2();
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMySocket_DebugDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RICHEDIT21, mEdit);
	DDX_Control(pDX, IDRUN, m_BtnRun);
}

BEGIN_MESSAGE_MAP(CMySocket_DebugDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDRUN, &CMySocket_DebugDlg::OnBnClickedRun)
END_MESSAGE_MAP()


// CMySocket_DebugDlg メッセージ ハンドラ

BOOL CMySocket_DebugDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// "バージョン情報..." メニューをシステム メニューに追加します。

	// IDM_ABOUTBOX は、システム コマンドの範囲内になければなりません。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// このダイアログのアイコンを設定します。アプリケーションのメイン ウィンドウがダイアログでない場合、
	//  Framework は、この設定を自動的に行います。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンの設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンの設定

	// TODO: 初期化をここに追加します。

	return TRUE;  // フォーカスをコントロールに設定した場合を除き、TRUE を返します。
}

void CMySocket_DebugDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// ダイアログに最小化ボタンを追加する場合、アイコンを描画するための
//  下のコードが必要です。ドキュメント/ビュー モデルを使う MFC アプリケーションの場合、
//  これは、Framework によって自動的に設定されます。

void CMySocket_DebugDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// クライアントの四角形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンの描画
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// ユーザーが最小化したウィンドウをドラッグしているときに表示するカーソルを取得するために、
//  システムがこの関数を呼び出します。
HCURSOR CMySocket_DebugDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMySocket_DebugDlg::OnBnClickedRun()
{
	this->m_BtnRun.EnableWindow(FALSE);

	this->mRunTest();

	this->m_BtnRun.EnableWindow(TRUE);
}

void CMySocket_DebugDlg::mRunTest()
{
	CPPUNIT_NS::TestSuite *it;
	// テスト結果を文字列ストリームに流し入れ、
	USHORT p = 0;

	it = all_test[p];
	while(it != NULL)
	{
		std::stringstream stream;

		// 初期化
		stream.str("");

		// イベント・マネージャとテスト・コントローラを生成する
		CPPUNIT_NS::TestResult controller;
		// テスト結果収集リスナをコントローラにアタッチする
		CPPUNIT_NS::TestResultCollector result;
		controller.addListener( &result );

		CPPUNIT_NS::Outputter *outputter = new CPPUNIT_NS::TextOutputter(&result, stream);

		// テストランナー
		CPPUNIT_NS::TextUi::TestRunner runner;
		runner.setOutputter(outputter);
		runner.addTest( it );

		stream << "**** Start   TEST **** (" << it->getName() << ")\n";
		runner.run( controller );
		outputter->write();
		stream << "**** Finish  TEST ****\n";

		// 出力
		char buff[_TEST_BUF_SIZE];
		stream.seekg(0);
		while(!stream.eof())
		{
			stream.getline(buff, _TEST_BUF_SIZE - 2);
			size_t s_len = strlen(buff);
			buff[s_len] = '\x0d';
			buff[s_len + 1] = '\x0a';
			buff[s_len + 2] = '\0';
			CString out(buff);
			this->mAddLogMsg(&out);
		}
		this->Invalidate(TRUE);

		it = all_test[++p];
	}
}

void CMySocket_DebugDlg::mAddLogMsg(CString *msg)
{
	INT len = this->mEdit.GetWindowTextLength();
	this->mEdit.SetSel(len, len);
	this->mEdit.ReplaceSel(*msg);
}
