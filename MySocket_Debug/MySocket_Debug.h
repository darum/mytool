// MySocket_Debug.h : PROJECT_NAME アプリケーションのメイン ヘッダー ファイルです。
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CMySocket_DebugApp:
// このクラスの実装については、MySocket_Debug.cpp を参照してください。
//

class CMySocket_DebugApp : public CWinApp
{
public:
	CMySocket_DebugApp();

// オーバーライド
	public:
	virtual BOOL InitInstance();

// 実装

	DECLARE_MESSAGE_MAP()
};

extern CMySocket_DebugApp theApp;