// MySocket_DebugDlg.h : ヘッダー ファイル
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CMySocket_DebugDlg ダイアログ
class CMySocket_DebugDlg : public CDialog
{
// コンストラクション
public:
	CMySocket_DebugDlg(CWnd* pParent = NULL);	// 標準コンストラクタ

// ダイアログ データ
	enum { IDD = IDD_MYSOCKET_DEBUG_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV サポート


// 実装
protected:
	HICON m_hIcon;

	// 生成された、メッセージ割り当て関数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	CRichEditCtrl mEdit;

	void mRunTest(void);
	void mAddLogMsg(CString *msg);
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedRun();
protected:
	CButton m_BtnRun;
};
