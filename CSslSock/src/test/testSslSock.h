#pragma once

#include "gtest/gtest.h"

#include "SslSock.h"

using namespace std;

class CSslSockTest : public ::testing::Test
{
public:
	CSslSockTest();
	~CSslSockTest();

	void SetUp();
	void TearDown();

protected:
	CSslSock *pSslSock;
};
