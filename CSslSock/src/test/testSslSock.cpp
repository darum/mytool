#include "testSslSock.h"

CSslSockTest::CSslSockTest()
{
}

CSslSockTest::~CSslSockTest()
{
}

void CSslSockTest::SetUp()
{
	this->pSslSock = new CSslSock();
}

void CSslSockTest::TearDown()
{
	delete(this->pSslSock);
}

TEST_F(CSslSockTest, test01_SendRecv)
{
  //	ASSERT_EQ(true, this->pSslSock->OpenCli(AF_INET, SOCK_STREAM, "192.168.1.100", 443));
	ASSERT_EQ(true, this->pSslSock->OpenCli(AF_INET, SOCK_STREAM, "darum.dip.jp", 443));

	/* 通常送受信 */
	string request = "GET /darum/pub/test.txt HTTP/1.1\r\n\r\n";
	size_t szAct;
	bool rc = this->pSslSock->Send(request.c_str(), request.size(), 0, &szAct);
	//	short int err_code = this->pSslSock->GetLastError();
	char errMsg[1024];
	this->pSslSock->GetErrorMessage(errMsg, 1024);
	ASSERT_EQ(true, rc) << errMsg;
	ASSERT_EQ(request.size(), szAct);

	char recv_msg[2048];
	ASSERT_EQ(true, this->pSslSock->Recv(recv_msg, 2048));
	ASSERT_EQ(true, this->pSslSock->Recv(recv_msg, 2048));

	/* 切断済み */
	int szRecvAct;
	recv_msg[0] = '\0';
	ASSERT_EQ(true, this->pSslSock->Recv(recv_msg, 2048, 0, (ssize_t*)&szRecvAct));
	ASSERT_EQ(-1, szRecvAct);
	ASSERT_EQ((unsigned char)SOCK_STAT_CLOSE, this->pSslSock->GetConStat());

	ASSERT_EQ(false, this->pSslSock->Close());
	ASSERT_EQ(SOCK_ALREADY_CLOSE, this->pSslSock->GetLastError());
	
}

TEST_F(CSslSockTest, test02_OpenClose)
{
	bool ret;
	short int err;
	unsigned char con;

	ret = pSslSock->OpenCli( AF_INET, SOCK_STREAM, "darum.darum", 23 );
	err = pSslSock->GetLastError();
	ASSERT_EQ(false, ret );
	ASSERT_EQ( SOCK_ADDR_ERROR, err );
	
	ret = pSslSock->OpenCli( AF_INET, SOCK_STREAM, "photozou.jp", 443 );
	con = pSslSock->GetConStat();
	ASSERT_EQ(true, ret );
	ASSERT_EQ( (int)SOCK_STAT_CONNECT, (int)con );

	ret = pSslSock->OpenCli( AF_INET, SOCK_STREAM, "darum.darum", 23 );
	err = pSslSock->GetLastError();
	ASSERT_EQ(false, ret );
	ASSERT_EQ( SOCK_CONNECT_EXIST, err );
	
	ret = pSslSock->Close();
	ASSERT_EQ(true, ret );
}
