#include "stdafx.h"
#include "SslSock.h"

/** コンストラクタ
 *
 */
CSslSock::CSslSock(void)
:m_ssl(NULL), m_ctx(NULL)
{
}

/** デストラクタ
 *
 */
CSslSock::~CSslSock(void)
{
}

/** クライアント接続
 *
 */
bool CSslSock::OpenCli(int family, int type, const char *addr, const unsigned short port)
{
	bool ret = true;

	// Check Init Status	
	if(this->m_ssl || this->m_ctx)
	{
		this->m_error = SSLSOCK_ALREADY_INIT;
		ret = false;
	}
	
	// Socket 接続
	ret = CSock::OpenCli(family, type, addr, port);
	if(ret == true)
	{
		int rc;	
		// Open SSL Socket
		rc = m_sslOpen();
		if(rc != 0)
		{
			m_error = rc;
			ret = false;
		}
	}

	if( ret == true)
	{
		// Set fd to SSLハンドル
		int fd = (int)m_getFd();

		if( SSL_set_fd( this->m_ssl, fd ) == 0 )
		{
			this->m_error = SSLSOCK_INIT_ERR;
			return false;
		}
	}

	if( ret == true)
	{
		// Init PRNG
		RAND_poll();
		while( RAND_status() == 0 )
		{
			unsigned short rand_ret = rand() % 65536;
			RAND_seed( &rand_ret, sizeof(rand_ret) );
		}
	}

	if(ret == true)
	{
		if(SSL_connect(this->m_ssl) < 1)
		{
			this->m_error = SSLSOCK_CON_ERR;
			ret = false;
		}
	}

	return ret;
}

bool CSslSock::Close()
{
	bool ret = true;
	int rc;

	if(this->m_conStat == SOCK_STAT_CLOSE)
	{
		// クローズ済み
		this->m_error = SOCK_ALREADY_CLOSE;
		ret = false;
	}


	if(ret == true)
	{
		if((rc = m_sslClose()) != SOCK_NO_ERROR)
		{
			this->m_error = rc;
			ret = false;
		}
	}

	if(ret == true)
	{
		rc = this->m_close();
		if(rc != SOCK_NO_ERROR)
		{
			this->m_error = rc;
			ret = false;
		}
	}

	return ret;

}

/** SSL Recv Method
 * @param [out] buf 受信バッファ
 * @param [in] count bufのサイズ
 * @param [in] flags 現在は意味がない
 * @param [out] szRecv 受信したサイズ。NULLの場合は値を返さない。切断時は-1
 * @param [in] timeout タイムアウト時間。NULLの場合は無限待ち。0指定ですぐ返す
 * @return true OK (切断時もOKで返す)
 * @return false NG. GetLastError()でエラー情報を取得できる。タイムアウト時もfalseとなる。
 */
bool CSslSock::Recv(void *buf, size_t count, int flags, ssize_t *szRecv, timeval *timeout)
{
//	fd_set fds;
	int rc;
	bool ret = true;
	bool rvFlag = false;
//	timeval minimum = {0, 1};
//	unsigned long cnt;

#if 0
	if((timeout != NULL) && ((timeout->tv_sec | timeout->tv_usec) == 0))
	{
		FD_ZERO(&fds);
		FD_SET(this->m_getFd(), &fds);

		rc = select(2, &fds, NULL, NULL, timeout);
		if(rc == MYSOCK_ERR)
		{
#ifdef _WIN32 
			this->m_error = WSAGetLastError();
#else
			this->m_error = errno;
#endif
		}
		ret = false;
	}
	else
	{
		if(timeout == NULL)
			cnt = (unsigned long)~0;
		else
			cnt = timeout->tv_sec * 1000000 + timeout->tv_usec;
		
		while(cnt)
		{
			FD_ZERO(&fds);
			FD_SET(this->m_getFd(), &fds);

			/* Time out Detected */
			rc = select(2, &fds, NULL, NULL, &minimum);
			if(rc == MYSOCK_ERR)
			{		/* Socket Error */
#ifdef _WIn32
				this->m_error = WSAGetLastError();
#else
				this->m_error = errno;
#endif
				ret = false;
				break;
			}
			else if(rc == 0)
			{
				if(SSL_pending(this->m_ssl) != 0)
				{		/* Data exists on SSL buffer */
					rvFlag = true;
					break;
				}
			}
			else 
			{
				/* Receive Data */
				rvFlag = true;
				break;
			}
			if(timeout != NULL)
				cnt--;
		}
	}
#else
	ret = true;
	rvFlag = true;
#endif
	if(ret == true)
	{
		if(rvFlag == true)
		{
			/* Ready receiving data */
			// Receive Ready
			rc = SSL_read(this->m_ssl, buf, (int)count);
			if(rc == 0)
			{
				// 切断
				if(szRecv != NULL)	*szRecv = -1;
				this->m_sslClose();
				this->m_close();
				
				ret = true;
			}
			else if(rc < 0)
			{
				// エラー
				if(szRecv != NULL)	*szRecv = 0;
				this->m_error = SSL_get_error(this->m_ssl, rc);
				ret = false;
			}
			else
			{
				// 正常受信
				if(szRecv != NULL)
				{
					*szRecv = rc;
				}
				ret = true;
			}
		}
		else
		{		/* Timeout */
			ret = false;
			this->m_error = SOCK_RECV_TIMEOUT;		// Timeoutをセット
		}
	}

	return ret;
}

/**
 *
 */
bool CSslSock::Send(const void *buf, size_t count, int flags, size_t *szSend)
{
	int rc;
	bool ret;

	rc = SSL_write(this->m_ssl, buf, (int)count);

	if(rc > 0)
	{
		// Success
		if(szSend != NULL)	*szSend = rc;
		ret = true;
	}
	else
	{
		this->m_error = SSL_get_error(this->m_ssl, rc);
//		this->m_error = ERR_get_error();
		ret = false;
	}

	return ret;

}

char *CSslSock::GetErrorMessage(char *buf, size_t len)
{
	ERR_error_string_n(ERR_get_error(), buf, len);
	return buf;
}

/////////////////////////////////////////////////////////////////////
/* Protected/Private Method */
int CSslSock::m_sslOpen()
{
	int ret = 0;
	
	if( ret == 0 )
	{
		/* Error Message関連初期化 */
		SSL_load_error_strings();
		ERR_load_crypto_strings();

		/* Library初期化 */
		SSL_library_init();
	}
	
	if( ret == 0 )
	{
		// CTX Create
		m_ctx = SSL_CTX_new(SSLv23_client_method());	// Select SSL Version
		if(m_ctx == NULL)
		{
			ret = 1;
		}
	}
	
	if(ret == 0)
	{
		// Create SSL
		this->m_ssl = SSL_new(m_ctx);
		if( this->m_ssl == NULL )
		{
			ret = 2;
		}
	}

	return ret;
}	

int CSslSock::m_close()
{
	// SSL Closeの確認
	if(this->m_conStat != SOCK_STAT_SSLCLOSE)
	{
		this->m_sslClose();
	}

	return CSock::m_close();
}

int CSslSock::m_sslClose()
{
	int ret=0, rc;
	
	rc = SSL_shutdown(m_ssl);
	if(rc == -1)
	{
		ret = SSL_get_error(m_ssl, rc);
	}
	
	/* リソースの解放 */
	SSL_free(this->m_ssl);
	SSL_CTX_free(this->m_ctx);
	ERR_free_strings();
	this->m_ssl = NULL;
	this->m_ctx = NULL;

	this->m_conStat = SOCK_STAT_SSLCLOSE;	// SSL Close 状態

	return ret;
}

#if 0
SOCK_DESC CSslSock::m_getFd()
{
	return this->m_fd;
}
#endif
