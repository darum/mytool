#pragma once

#include "gtest/gtest.h"

#include "Sock.h"

using namespace std;

class CSockTest : public ::testing::Test
{

public:
	CSockTest();
	~CSockTest();

	void SetUp();
	void TearDown();

protected:
	CSock *pCsock;
};

#define RECV_DATA_SIZE 32
