#include "stdafx.h"

#include "testSock.h"

#ifndef _WIN32
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

//#include <iostream>

CSockTest::CSockTest(void)
{

}

CSockTest::~CSockTest(void)
{

}

void CSockTest::SetUp()
{
	pCsock = new CSock();
}

void CSockTest::TearDown()
{
	delete pCsock;
}


TEST_F(CSockTest, test_01)
{
	CSock tcpTest1, tcpTest2, tcpNG1, tcpNG2;
	bool ret;
	short int err;
	unsigned char conStat;

	ret = tcpTest1.OpenSvr( AF_INET, SOCK_STREAM, 30000 );
	ASSERT_EQ(true, ret);

	ret = tcpTest2.OpenSvr( AF_INET, SOCK_STREAM, 30001, "127.0.0.1" );
	conStat = tcpTest2.GetConStat();
	ASSERT_EQ(true, ret);
	ASSERT_EQ( (int)SOCK_STAT_OPEN, (int)conStat );

	ret = tcpTest2.OpenSvr( AF_INET, SOCK_STREAM, 30000 );
	err = tcpTest2.GetLastError();
	ASSERT_EQ(false, ret);
	ASSERT_EQ( SOCK_CONNECT_EXIST, err );

#if defined(__unix__) && !defined(__CYGWIN__)
	// System Port cannot listen
	ret = tcpNG1.OpenSvr( AF_INET, SOCK_STREAM, 512 );
	err = tcpNG1.GetLastError();
	ASSERT_EQ(false, ret);
	ASSERT_NE(0, err);
#endif

	ret = tcpNG2.OpenSvr( AF_INET, SOCK_STREAM, 30002, "darumdarum" );
	err = tcpNG2.GetLastError();
	ASSERT_EQ(false, ret);
	ASSERT_NE(0, err);

}

TEST_F(CSockTest, test02_Listen)
{
	bool ret;
	short int err;
	unsigned char conStat;

	ret = pCsock->Listen();
	err = pCsock->GetLastError();
	ASSERT_EQ(false, ret);
	ASSERT_EQ( SOCK_CONNECT_NOOPEN, err );

	ret = pCsock->OpenSvr( AF_INET, SOCK_STREAM, 30100 );
	ASSERT_EQ(true, ret);
	ret = pCsock->Listen();
	conStat = pCsock->GetConStat();
	ASSERT_EQ(true, ret);
	ASSERT_EQ( (int)SOCK_STAT_LISTEN, (int)conStat );
}

TEST_F(CSockTest, test021_ClientOpen)
{
	bool ret;
	short int err;
	unsigned char con;

	ret = pCsock->OpenCli( AF_INET, SOCK_STREAM, "darum.darum", 23 );
	err = pCsock->GetLastError();
	ASSERT_EQ(false, ret);
	ASSERT_NE(SOCK_NO_ERROR, err);

	ret = pCsock->OpenCli( AF_INET, SOCK_STREAM, "www.yahoo.co.jp", 80 );
	con = pCsock->GetConStat();
	ASSERT_EQ(true, ret);
	ASSERT_EQ( (int)SOCK_STAT_CONNECT, (int)con );

	ret = pCsock->OpenCli( AF_INET, SOCK_STREAM, "darum.darum", 23 );
	err = pCsock->GetLastError();
	ASSERT_EQ(false, ret);
	ASSERT_EQ( SOCK_CONNECT_EXIST, err );

}

TEST_F(CSockTest, test03_Accept)
{
	bool ret;
	short int err;
	unsigned char conStat, conStat2;
	CSock *conSock, cliSock;

	conSock = pCsock->Accept();
	err = pCsock->GetLastError();
	ASSERT_EQ((CSock*)NULL, conSock);
	ASSERT_EQ( err, SOCK_CONNECT_NOOPEN );

	ret = pCsock->OpenSvr( AF_INET, SOCK_STREAM, 30200 );
	ASSERT_EQ(true, ret);
	ret = pCsock->Listen();
	ASSERT_EQ(true, ret);

	ret = cliSock.OpenCli( AF_INET, SOCK_STREAM, "127.0.0.1", 30200 );
	ASSERT_EQ(true, ret);
	
	conSock = pCsock->Accept();
	conStat = pCsock->GetConStat();
	ASSERT_EQ( (int)SOCK_STAT_LISTEN, (int)conStat );
	ASSERT_NE((CSock*)NULL, conSock);
	conStat2 = conSock->GetConStat();
	ASSERT_EQ( (int)SOCK_STAT_CONNECT, (int)conStat2 );
}

TEST_F(CSockTest, test04_Close)
{
	bool ret;
	short int err;
	unsigned char conStat;

	// Close test(Abnormal) if the socket is not open
	ret = pCsock->Close();
	err = pCsock->GetLastError();
	ASSERT_EQ(false, ret);
	ASSERT_EQ(SOCK_ALREADY_CLOSE, err);
	
	ret = pCsock->OpenSvr( AF_INET, SOCK_STREAM, 30300 );
	ASSERT_EQ(true, ret);
	ret = pCsock->Close();
	conStat = pCsock->GetConStat();
	ASSERT_EQ(true, ret);
	ASSERT_EQ( (int)SOCK_STAT_CLOSE, (int)conStat );
	
}

TEST_F(CSockTest, test05_SetFd_GetPeerAddress)
{
	bool ret;
	short int err;
	unsigned char conStat;
	CSock *conSock, cliSock;
	struct sockaddr addr;
	struct sockaddr_in *pAddr;
	stringstream smsg;
	char cAddr[16], *tmp;

	pAddr = (struct sockaddr_in *)&addr;
	conSock = new CSock();

	// SetFd(): non-socket descriptor Regist
#if defined( _WIN32)
	ret = conSock->SetFd((SOCKET)2);
#elif (defined(__APPLE__) || defined(__CYGWIN__) || defined(__FreeBSD__))
	ret = conSock->SetFd( stderr->_file);
#else
	ret = conSock->SetFd( stderr->_fileno );	//2 = stderr
#endif
	err = conSock->GetLastError();
	ASSERT_EQ(false, ret);
	smsg << "err=" << err;
	short int checkCode;
#ifdef _WIN32
	checkCode = WSAENOTSOCK;
#else
	checkCode = ENOTSOCK;
#endif
	ASSERT_EQ( checkCode, err ) << smsg.str();

	// GetPeerAddress(): not connected
	ret = pCsock->GetPeerAddress( addr );
	err = pCsock->GetLastError();
	ASSERT_EQ(false, ret);
	ASSERT_EQ( SOCK_CONNECT_NOOPEN, err );

	//
	ret = pCsock->OpenSvr( AF_INET, SOCK_STREAM, 30400 );
	ASSERT_EQ(true, ret);
	ret = pCsock->Listen();
	ASSERT_EQ(true, ret);

	ret = cliSock.OpenCli( AF_INET, SOCK_STREAM, "127.0.0.1", 30400 );
	ASSERT_EQ(true, ret);
	
	conSock = pCsock->Accept();
	conStat = conSock->GetConStat();
	ASSERT_NE((CSock*)NULL, conSock);
	ASSERT_EQ( (int)SOCK_STAT_CONNECT, (int)conStat );
	
	// SetFd(): Already Registed
	ret = conSock->SetFd( 10 );
	err = conSock->GetLastError();
	ASSERT_EQ(false, ret);
	smsg << "err=" << err;
	ASSERT_EQ(SOCK_CONNECT_EXIST, err) << smsg.str();
	
	// GetPeerAddress()
	ret = conSock->GetPeerAddress( addr );
	ASSERT_EQ(true, ret);
	tmp = inet_ntoa( pAddr->sin_addr );
	memcpy( cAddr, tmp, 16 );
	ASSERT_EQ(0, strcmp( cAddr, "127.0.0.1" )) << cAddr;
}

TEST_F(CSockTest, test06_SendRecv)
{
	bool ret;
	short int err;
	unsigned char conStat;
	CSock *conSock, cliSock;
	stringstream smsg, errmsg;
	size_t szSend, szActSend;
	ssize_t szActRecv;
	char recvData[RECV_DATA_SIZE];
	string strRvData;

	struct timeval no_to = { 0L, 0L };	// Not Blocking

	conSock = new CSock();

	////
	// Case 6-1
	// Normally send-recv Test
	//
	// Server Setup
	ret = pCsock->OpenSvr( AF_INET, SOCK_STREAM, 30500 );
	ASSERT_EQ(true, ret);
	ret = pCsock->Listen();
	ASSERT_EQ(true, ret);

	// Client Setup
	ret = cliSock.OpenCli( AF_INET, SOCK_STREAM, "127.0.0.1", 30500 );
	ASSERT_EQ(true, ret);
	
	// Established Procedure
	conSock = pCsock->Accept();
	conStat = conSock->GetConStat();
	ASSERT_NE((CSock*)NULL, conSock);
	ASSERT_EQ( (int)SOCK_STAT_CONNECT, (int)conStat );
	conStat = cliSock.GetConStat();
	ASSERT_EQ( (int)SOCK_STAT_CONNECT, (int)conStat );
	
	// Send Data from Client
	smsg << "DATA1: DATA DATA";
	szSend = smsg.str().length();
	// Send Data (Client -> Server) (with Block until data received)
	ret = cliSock.Send( (const char *)smsg.str().c_str(), szSend, 0, &szActSend );
	ASSERT_EQ(true, ret);
	ret = conSock->Recv( recvData, (size_t)RECV_DATA_SIZE, 0, &szActRecv );
	ASSERT_EQ(true, ret);

	ASSERT_EQ(szSend, szActSend);
	recvData[szActRecv] = '\0';
	errmsg << "Recv Data: [" << string(recvData) << "]";
	ASSERT_EQ(0, strcmp( recvData, smsg.str().c_str() )) << errmsg.str();

	////
	// Case 6-2
	// Send Message, Receive Data by 1Byte
	//
	// Buffer Clear
	memset( recvData, '\0', RECV_DATA_SIZE );
	// Sned Data from Client
	ret = cliSock.Send( (const char *)smsg.str().c_str(), szSend, 0, &szActSend );
	ASSERT_EQ(true, ret);
	strRvData.clear();
	// Receive Data by 1Byte
	while( true )
	{
		// Receive with timeout
//		ret = conSock->Recv( recvData, 1, MSG_DONTWAIT, &szActRecv );
		ret = conSock->Recv( recvData, 1, 0, &szActRecv, &no_to );
		if( ret == false )
		{
			// All data has received
			err = conSock->GetLastError();
			ASSERT_EQ( (int)SOCK_RECV_TIMEOUT, (int)err );
			break;
		}

		strRvData.append( 1, recvData[0] );
	}

	ret = cliSock.Close();
	ASSERT_EQ(true, ret);
	conStat = cliSock.GetConStat();
	ASSERT_EQ( (int)SOCK_STAT_CLOSE, (int)conStat );
//	ret = conSock->Recv( recvData, 1, MSG_DONTWAIT, &szActRecv );
	// 対向がすでに切断された状態でrecv()
	ret = conSock->Recv( recvData, 1, 0, &szActRecv, &no_to );
	ASSERT_EQ(true, ret);
	ASSERT_EQ( -1, (int)szActRecv );
	ret = conSock->Close();
	ASSERT_EQ(false, ret);
	err = conSock->GetLastError();
#if 1
	ASSERT_EQ( SOCK_ALREADY_CLOSE, err );
#else
	ASSERT_EQ( (int)EBADF, (int)err );
#endif
	conStat = conSock->GetConStat();
	ASSERT_EQ( (int)SOCK_STAT_CLOSE, (int)conStat );
	conStat = pCsock->GetConStat();
	ASSERT_EQ( (int)SOCK_STAT_LISTEN, (int)conStat );

	errmsg << "Recv Data: [" << string(recvData) << "]";
	ASSERT_EQ(0, strRvData.compare( smsg.str() )) << errmsg.str();
}
