#include "Sock.h"

#define SET_ERROR(A)	m_error = (A); ret = false;

CSock::CSock()
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(winsockVer, &wsaData);
#endif

	m_pAddr = NULL;
	m_conStat = SOCK_STAT_CLOSE;
	m_error = 0;

}

CSock::~CSock()
{
	if( m_conStat != SOCK_STAT_CLOSE )
	{
		this->m_close();
	}
#ifdef _WIN32
	WSACleanup();
#endif
}

/** 
 * @brief Open Socket for Server
 * @param family Protocol Family (AF_INET, ...)
 * @param type Socket semantics (SOCK_STREAM, SOCK_DGRAM, ...)
 * @param port Listening Port Number
 * @param addr Acceptable Address
 * @return true OK
 * @return false NG
 */ 
bool CSock::OpenSvr( int family, int type, unsigned short port, const char *addr )
{
	bool ret = true;
//	int fd;
	struct sockaddr_in saddr;
	struct hostent *hp;

	while(true)
	{
		if( m_conStat != SOCK_STAT_CLOSE)
		{										// Already Used
			m_error = SOCK_CONNECT_EXIST;
			ret = false;
			break;
		}


		//
		// Socket
		//
		ret = m_socket( family, type );
		if( ret != true )
		{
			break;
		}

		//
		// Bind
		//
		memset( (char *)&saddr, 0, sizeof(saddr) );		// Clear Buffer
		saddr.sin_family = family;
		saddr.sin_port = htons(port);
		
		if( addr == NULL )
		{
			saddr.sin_addr.s_addr = INADDR_ANY;
		}
		else
		{
			hp = gethostbyname(addr);
			if( hp == NULL )
			{
				m_error = h_errno;
				ret = false;
				break;
			}
			memcpy( &saddr.sin_addr, hp->h_addr_list[0], hp->h_length );
		}

		if( bind( m_fd, (struct sockaddr *)&saddr, sizeof(saddr) ) < 0 )
		{
#ifdef _WIN32
			SET_ERROR(WSAGetLastError());
#else
			SET_ERROR(errno)
#endif
			break;
		}

		// Regist Address
		m_pAddr = (struct sockaddr *)malloc( sizeof(struct sockaddr) );
		if( m_pAddr == NULL )
		{
			m_error = SOCK_MEMORY_ERROR;
			ret = false;
			break;
		}
		memcpy( m_pAddr, &saddr, sizeof(sockaddr) );

		m_conStat = SOCK_STAT_OPEN;

		break;
	}

	return ret;
}

/**
 * @brief Open Socket for Client
 * @param [in] family Protocol family (AF_INET, ...)
 * @param [in] type Socket Semantics (SOCK_STREAM, SOCK_DGRAM, ...)
 * @param [in] addr Server Address
 * @param [in] port Server Port
 * @return true OK
 * @return false NG. Error Code is get by CSock::GetLastError()
*/
bool CSock::OpenCli( int family, int type, const char *addr, const unsigned short port )
{
	bool ret = true;
	struct sockaddr_in saddr;
	struct addrinfo *ai;
	int rc;
	
	while( true )
	{
		// Check Connect Status
		if( m_conStat != SOCK_STAT_CLOSE)
		{										// Already Used
			m_error = SOCK_CONNECT_EXIST;
			ret = false;
			break;
		}
		
		// Socket()
		ret = m_socket( family, type );
		if( ret == false )
		{
			break;
		}

		rc = getaddrinfo( addr, NULL, NULL, &ai );
		if( rc != 0 )
		{
#ifdef _WIN32
#if _MSC_VER < 1500
			SET_ERROR( rc );
#else
			switch( rc ){
			case WSAHOST_NOT_FOUND:
				SET_ERROR(SOCK_ADDR_ERROR);
				break;
			default:
				SET_ERROR( rc );
				break;
			}
#endif
#else
			switch( rc )
			{
			case EAI_SYSTEM:
				SET_ERROR( errno );
				break;
			case EAI_NONAME:
			case EAI_ADDRFAMILY:
				SET_ERROR( SOCK_ADDR_ERROR ); // アドレスがおかしい
				break;
			case EAI_MEMORY:
				SET_ERROR(SOCK_MEMORY_ERROR); // Memory Error
				break;
			default:
				SET_ERROR( rc );
				break;
			}
#endif
			break;
		}
		memcpy( &saddr, ai[0].ai_addr, sizeof(struct sockaddr_in) );
		freeaddrinfo( ai );
		saddr.sin_family = family;
		saddr.sin_port = htons(port);

		if( connect( m_fd, (struct sockaddr *)&saddr, sizeof(struct sockaddr) ) < 0 )
		{
#ifdef _WIN32
			SET_ERROR(WSAGetLastError());
#else
			SET_ERROR(errno)
#endif
			break;
		}

		m_conStat = SOCK_STAT_CONNECT;
		break;
	}

	return ret;
}

/**
 * @brief Get Last Error Code
 * @return SOCK_NO_ERROR No Errors
 * @return Others Error Code (Such as errno, WSAGetLastError())
 */
short int CSock::GetLastError()
{
	short int ret;
	ret = m_error;
	m_error = SOCK_NO_ERROR;

	return ret;
}

/**
 * @brief Listening Port
 * @return true OK
 * @return false NG
 */
bool CSock::Listen()
{
	bool ret = true;
	
	while(true)
	{
		// Status Check
		switch( m_conStat )
		{
		case SOCK_STAT_OPEN:
			break;
		case SOCK_STAT_CLOSE:
			m_error = SOCK_CONNECT_NOOPEN;
			ret = false;
			break;
		default:
			m_error = SOCK_CONNECT_EXIST;
			ret = false;
			break;
		}

		if( ret == false )
		{
			break;
		}


		//
		// Listen
		//
		if( listen( m_fd, SOMAXCONN ) < 0 )
		{
#ifdef _WIN32
			SET_ERROR(WSAGetLastError());
#else
			SET_ERROR(errno)
#endif
			break;
		}

		// Waiting accept
		m_conStat = SOCK_STAT_LISTEN;
		break;
	}

	return ret;
}

/**
 * @brief Descriptorのセット
 * 接続済みのDescriptorをセットし、クラスを利用する
 * @param [in] fd Descriptor
 * @return true OK
 * @return false NG
 */
bool CSock::SetFd( const SOCK_DESC fd )
{
	bool ret = true;

	struct sockaddr peerAddr;
	int szAddr;

	while( true )
	{
		// Status Check
		if( m_conStat != SOCK_STAT_CLOSE )
		{
			SET_ERROR(SOCK_CONNECT_EXIST);
			break;
		}

		szAddr = sizeof(peerAddr);
		// FD Statis check
		if( getpeername( fd, &peerAddr, (socklen_t *)&szAddr ) < 0 )
		{
#ifdef _WIN32
			SET_ERROR(WSAGetLastError());
#else
			SET_ERROR(errno)
#endif
			break;
		}

		// Regist Address Info.
		if( mSetAddr( &peerAddr ) == false )
		{
			ret = false;
			break;
		}

		// Regist Descriptor
		m_fd = fd;
		m_conStat = SOCK_STAT_CONNECT;

		break;
	}
	
	return ret;

}

/**
 * @brief 接続を受け入れる
 * @return 通信用CSockオブジェクト
 */
CSock *CSock::Accept()
{
	bool ret = true;
	CSock *retSock = NULL;
	struct sockaddr caddr;
	int lAddr;

	SOCK_DESC new_fd;
	
	while(true)
	{
		// Status Check
		if( m_conStat != SOCK_STAT_LISTEN )
		{
			SET_ERROR(SOCK_CONNECT_NOOPEN);
			break;
		}

		memset( &caddr, 0, sizeof(caddr) );
		lAddr = sizeof(caddr);
		new_fd = accept( m_fd, (struct sockaddr*)&caddr, (socklen_t *)&lAddr );
		if( new_fd < 0 )
		{
#ifdef _WIN32
			SET_ERROR(WSAGetLastError());
#else
			SET_ERROR(errno)
#endif
			break;
		}

		retSock = new CSock();
		if( retSock->SetFd( new_fd ) == false )
		{
			delete(retSock);
			retSock = NULL;
			SET_ERROR( SOCK_CREATE_ERROR );
			break;
		}

		break;
	}

	return retSock;
}

/**
 * @brief 接続状態の取得
 * @return 接続状態
 */
unsigned char CSock::GetConStat()
{
	return m_conStat;
}

/**
 * @brief Close Socket
 * @return true OK
 * @return false NG
 */
bool CSock::Close()
{
	bool ret = true;
	int i_ret;

	i_ret = m_close();
	if( i_ret != 0 )
	{
		ret = false;
		// Set error code into m_error is processed in m_close(); 
	}

	return ret;
	
}

/**
 * @brief Get Peer Address
 * @param [out] addr 接続先のPeer Address
 * @return true OK
 * @return false NG
 */
bool CSock::GetPeerAddress( struct sockaddr & addr )
{
	bool ret = true;
	
	while(true)
	{
		if( m_conStat != SOCK_STAT_CONNECT )
		{
			SET_ERROR( SOCK_CONNECT_NOOPEN );
			break;
		}

		// Unmatch Internal Status
		if( m_pAddr == NULL )
		{
			SET_ERROR( SOCK_INTERNAL_ERROR );
			break;
		}

		ret = mGetAddr(addr);
		break;
	}

	return ret;
}

/**
 * @brief データの受信
 * @param [out] buf 受信データの格納バッファ
 * @param [in] count バッファサイズ
 * @param [in] flags 受信フラグ
 * @param [out] szRecv 受信したデータサイズ（NULLの場合は格納しない）切断時は-1、エラー時は0
 * @param [in] timeout タイムアウト
 * @return true OK (切断時もOK)
 * @return false NG
 */
bool CSock::Recv( void *buf, size_t count, int flags, ssize_t *szRecv, struct timeval *timeout )
{
	bool ret = true;
	ssize_t szRet;

	while(true)
	{
		// Connection Status
		if( m_conStat != SOCK_STAT_CONNECT )
		{
			SET_ERROR( SOCK_CONNECT_NOOPEN );
			break;
		}

		szRet = m_read( buf, count, flags, timeout );
		if( szRet > 0 )
		{	// 正常受信
			if( szRecv != NULL )
			{
				*szRecv = szRet;
			}
		}
		else if( szRet == 0 )
		{	// 切断
			ret = true;
			if( szRecv != NULL )
			{
				*szRecv = -1;
			}
			m_close();
		}
		else
		{	// エラー
			ret = false;
			if( szRecv != NULL )
			{
				*szRecv = 0;
			}
			break;
		}

		break;
	}

	return ret;
}

/**
 * @brief データの送信
 * @param [in] buf 送信データ
 * @param [in] count 送信するサイズ
 * @param [in] flags 送信フラグ
 * @param [out] szSend 実際に送信したサイズ
 * @return true OK
 * @return false NG
 */
bool CSock::Send( const void *buf, size_t count, int flags, size_t *szSend )
{
	bool ret = true;
	ssize_t szRet;

	while( true )
	{
		if( m_conStat != SOCK_STAT_CONNECT )
		{
			SET_ERROR( SOCK_CONNECT_NOOPEN );
			break;
		}

		szRet = m_write( buf, count, flags );
		if( szRet > 0 )
		{
			if( szSend != NULL )
			{
				*szSend = szRet;
			}
		}
		else if( szRet == 0 )
		{
			*szSend = 0;
			m_close();
		}
		else
		{
#ifdef _WIN32
			SET_ERROR(WSAGetLastError());
#else
			SET_ERROR(errno)
#endif
			break;
		}

		break;
	}

	return ret;
}

// Protected method
ssize_t CSock::m_read( void *buf, size_t count, int flags, struct timeval *timeout )
{
	ssize_t ret = 0;

	while(true)
	{
		if( timeout != NULL )
		{
			// Timeout Check
			fd_set readfds;
			FD_ZERO(&readfds);	// Clear
			FD_SET( m_fd, &readfds );
			
			ret = select( FD_SETSIZE, &readfds, (fd_set *)NULL, (fd_set *)NULL, timeout );
			if( ret < 0 )
			{
				// Error Occured
#ifdef _WIN32
				m_error = WSAGetLastError();
#else
				m_error = errno;
#endif
				break;
			}
			else if( !FD_ISSET(m_fd, &readfds))
			{
				// timeout
				m_error = SOCK_RECV_TIMEOUT;
				ret = (ssize_t)-1;
				break;
			}
		}

#ifdef _WIN32
		ret = (ssize_t)recv( m_fd, (char *)buf, (int)count, flags );
#else
		ret = recv( m_fd, buf, count, flags );
#endif
		break;
	}
	return ret;
}

ssize_t CSock::m_write( const void *buf, size_t count, int flags )
{
#ifdef _WIN32
	return (ssize_t)send( m_fd, (const char *)buf, (int)count, flags );
#else
	return send( m_fd, buf, count, flags|MSG_NOSIGNAL );
#endif
}

bool CSock::mSetAddr( const struct sockaddr * addr )
{
	bool ret = true;

	while( true )
	{
		
		if( m_pAddr != NULL )
		{
			free(m_pAddr);
			m_pAddr = NULL;
		}
		m_pAddr = (struct sockaddr*)malloc(sizeof(struct sockaddr));
		if( m_pAddr == NULL )
		{
			SET_ERROR(SOCK_MEMORY_ERROR);
			break;
		}
		memcpy( m_pAddr, addr, sizeof( struct sockaddr ) );

		break;
	}

	return ret;
}

bool CSock::mGetAddr( struct sockaddr &addr )
{
	bool ret = true;

	while( true )
	{
		memcpy( &addr, m_pAddr, sizeof(struct sockaddr) );

		break;
	}

	return ret;
}

// If error occurred, m_error is set in this function.
int CSock::m_close()
{
	int ret;

#ifdef _WIN32
	ret = closesocket(m_fd);
	if( ret == SOCKET_ERROR )
	{
		int wsaErr = WSAGetLastError();
		if( wsaErr == WSAENOTSOCK )
		{
			// Socket is Invalid
			m_conStat = SOCK_STAT_CLOSE;
			m_error = SOCK_ALREADY_CLOSE;
		}
		else
		{
			m_error = wsaErr;
		}
	}
	else
	{
		// Close Successful
		m_conStat = SOCK_STAT_CLOSE;
	}
#else
	ret = close(m_fd);
	if( ret == 0 )
	{
		m_conStat = SOCK_STAT_CLOSE;
	}
	else
	{
		// close() ERROR
		if( errno == EBADF )
		{
			m_error = SOCK_ALREADY_CLOSE;
		m_conStat = SOCK_STAT_CLOSE;
		}
		else
		{
			m_error = errno;
		}
	}
#endif

	return ret;
}

/**
 * @brief ソケットの接続
 * @param [in] family Protocol family (AF_INET, ...)
 * @param [in] type Socket Semantics (SOCK_STREAM, SOCK_DGRAM, ...)
 * @return true OK
 * @return false NG
 */
bool CSock::m_socket( int family, int type )
{
	bool ret = true;
	SOCK_DESC fd;

	fd = socket( family, type, 0 );
	if( fd == -1 )
	{
#ifdef _WIN32
		SET_ERROR(WSAGetLastError());
#else
		SET_ERROR(errno)
#endif
	}
	m_fd = fd;							// Regist Descriptor
	
	return ret;
}

SOCK_DESC CSock::m_getFd()

{

	return this->m_fd;

}

