#include "log.h"

#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <time.h>
#include <string.h>

#ifdef _WIN32
#include <tchar.h>
#endif

#define BUFSIZE 256

const char LVStr[][CLOG_MAX_szLVStr+1] = {{"ERROR"}, {"WARN"}, {"INFO"}, {"DEBUG"},};

using namespace myLog;

CLog::CLog( const string &file )
{
	m_fname = file;
	m_outLv = LV_DEBUG;
}

CLog::~CLog()
{
}

/**
 * @brief Log Message Output
 * @param level Output Level
 * @param format Message
 * @return LOG_PROC_OK OK
 * @return >0 Error (= errno)
 */
int CLog::Write( unsigned char level, const char *format, ... )
{
	int ret = LOG_PROC_OK;

	if( level <= m_outLv )
	{
		FILE *fp;
#ifdef _DEBUG
		if( strncmp( m_fname.c_str(), LOG_STDOUT, 1 ) == 0 )
		{
			fp = stdout;
		}
		else
#endif
		{
#if _MSC_VER >= 1400		// VC2005 or later
			int rc;
			rc = fopen_s( &fp, m_fname.c_str(), "at" );
			if( rc != 0 )
			{
				ret = rc;
				return ret;
			}
#else
			fp = fopen( m_fname.c_str(), "at" );
			if( fp == NULL )
			{
				ret = errno;
				return ret;
			}
#endif
		}
		
		time_t timer;
		time(&timer);
		struct tm _tm;
#if defined(__MINGW32__)
		// ToDo: ����
#elif defined(_WIN32)
		localtime_s( &_tm, &timer );
#else
		localtime_r( &timer, &_tm );
#endif
		char tmBuf[BUFSIZE];
		strftime( tmBuf, BUFSIZE, "%m/%d %H:%M:%S", &_tm );
		fprintf( fp, "%s [%-*s] ", tmBuf, CLOG_MAX_szLVStr, LVStr[level] );

		va_list list;
		va_start(list,format);
		vfprintf(fp, format, list);
		va_end(list);
		fputc( '\n', fp );
		
		fflush(fp);
		fclose(fp);
	}

	return ret;
}

int CLog::DebugWrite( const char *format, ... )
{
#ifdef _DEBUG
	FILE *fp;
#if _MSC_VER >= 1400		// VC2005 or later
	int rc;
	rc = fopen_s( &fp, m_fname.c_str(), "at" );
	if( rc != 0 )
	{
		return rc;
	}
#else
	fp = fopen( m_fname.c_str(), "a+t" );
	if( fp == NULL )
	{
		return errno;
	}
#endif

	fprintf( fp, "##DEBUG##: " );
	va_list list;
	va_start(list,format);
	vfprintf(fp, format, list);
	va_end(list);
	fputc( '\n', fp );
	
	fflush(fp);
	fclose(fp);

	return LOG_PROC_OK;
#endif	
}

