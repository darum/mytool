#include "stdafx.h"
#include "testLog.h"

#include <fstream>
#include <string.h>

CLogTest::CLogTest(void)
{

}

CLogTest::~CLogTest(void)
{

}

void CLogTest::SetUp()
{
//	m_pLog = new CLog("a.log");
}

void CLogTest::TearDown()
{
//	delete m_pLog;
}


TEST_F(CLogTest, test_01)
{
	m_pLog = new CLog("test01.log");

	m_pLog->Write(CLog::LV_INFO, "Message01");
	m_pLog->DebugWrite( "DebugMessage" );
	
	delete m_pLog;

	ifstream ifs;
//	string line;
	char tmp[256], lv[8], msg[256];
	ifs.open( "test01.log" );
	ifs.getline( tmp, 256 );

	int mon, day, hh, mm, ss;
#ifdef _WIN32
#if _MSC_VER >= 1400
	sscanf_s(tmp, "%d/%d %d:%d:%d [%5s]: %s", &mon, &day, &hh, &mm, &ss, lv, 8, msg, 256 );
#else
	sscanf( tmp, "%d/%d %d:%d:%d [%5s]: %s", &mon, &day, &hh, &mm, &ss, lv, msg );
#endif
#else
	sscanf( tmp, "%d/%d %d:%d:%d [%5s]: %s", &mon, &day, &hh, &mm, &ss, lv, msg );
#endif
	ASSERT_LE(1, mon);
	ASSERT_LE(mon, 12);
	ASSERT_LE(1, day);
	ASSERT_LE(day, 31);
	ASSERT_LE(0, hh);
	ASSERT_LE(hh, 24);
	ASSERT_LE(0, mm);
	ASSERT_LE(mm, 60);
	ASSERT_LE(0, ss);
	ASSERT_LE(ss, 60);
	ASSERT_EQ(0, strncmp( tmp+16, "INFO ", 5 )) << tmp;
	ASSERT_EQ(0, strncmp( tmp+22, " Message01", 10 ));

	ifs.getline( tmp, 256 );
	ASSERT_EQ( 0, strncmp( tmp, "##DEBUG##", 9 ) );
	
}

