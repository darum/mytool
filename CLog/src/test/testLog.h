#pragma once

#include "gtest/gtest.h"

#include "log.h"

using namespace std;
using namespace myLog;

class CLogTest : public ::testing::Test
{
public:
	CLogTest();
	~CLogTest();

	void SetUp();
	void TearDown();

protected:
	myLog::CLog *m_pLog;
};

#define RECV_DATA_SIZE 32
