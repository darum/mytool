#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

#include "testMain.h"

#ifdef _WIN32
int main(void)
#else
int main( int argc, char *argv[] )
#endif
{
  // イベント・マネージャとテスト・コント��充ラを生成す��E
  CPPUNIT_NS::TestResult controller;

  // テスト��E娘�集��E好覆鬟灰鵐肇�充ラにアタッチす��E
  CPPUNIT_NS::TestResultCollector result;
  controller.addListener( &result );

  // 「.」で進行状況を出力す��E�E好覆鬟▲織奪舛垢�E
  CPPUNIT_NS::BriefTestProgressListener progress;
  controller.addListener( &progress );

  // テスト・ランナーにテスト群を与え、テストす��E
  CPPUNIT_NS::TestRunner runner;
  runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
  runner.run( controller );

  // テスト��E未鯢現狃侘呂謀任�出す
  CPPUNIT_NS::CompilerOutputter outputter( &result, CPPUNIT_NS::stdCOut() );
  outputter.write();

  return result.wasSuccessful() ? 0 : 1;
}

