#include "stdafx.h"

#include "testCfgLoad.h"

// データファイルのディレクトリ
#ifdef _WIN32
// Note: ワークディレクトリをMyToolsへセットする
const std::string data_dir = string("CCfgLoad/src/test/datafile/");
#elif defined(__unix__) || defined(__APPLE__)
const std::string data_dir = string("test/datafile/");
#else
const std::string data_dir = string("datafile");
#endif

/////////////////////////////////////
// テストコード(本体)

// Test Class
CCfgLoadTest::CCfgLoadTest()
{

}

CCfgLoadTest::~CCfgLoadTest()
{

}

void CCfgLoadTest::SetUp()
{
	target = new CCfgLoad();
}

void CCfgLoadTest::TearDown()
{
	delete target;
}

TEST_F(CCfgLoadTest, test_CfgOpen)
{
	int ret;
	string fname;

	ret = target->CfgOpen( string( "nofile.ini" ) );
	ASSERT_EQ( CFG_ERROR_FOPEN, ret );

	fname = data_dir;
	fname += string("cfgOpen_01.txt");
	ret = target->CfgOpen(fname);
	ASSERT_EQ( CFG_PROC_OK, ret );
#ifndef _WIN32
	string sRet = target->GetCfgName();
	ASSERT_TRUE( sRet.compare(fname) == 0 );
#endif
	char fname_c[256];
	target->GetCfgName(fname_c, 256);
	stringstream ss;
	ss.str("");
	ss << "GetCfgName=[" << string(fname_c) << "]";
	ASSERT_EQ(0, fname.compare(fname_c)) << ss.str();

	ASSERT_EQ( 2, target->GetSecCnt() );
	ASSERT_EQ( 2, target->GetItemCnt(string("SEC1")) );

	ret = target->CfgOpen( data_dir + string("cfgOpen_02.txt") );
	ASSERT_EQ( CFG_ERROR_NOREC, ret );

	ret = target->CfgOpen(data_dir + string("cfgOpen_03.txt") );
	ASSERT_EQ( CFG_ERROR_NOREC, ret );

	ASSERT_EQ( CFG_ERROR_FORMAT, target->CfgOpen(data_dir +  string("cfgOpen_04.txt")));
	ASSERT_EQ( 4, (int)target->GetErrorLine() );
	ASSERT_EQ( 0, (int)target->GetErrorLine() );

	ASSERT_EQ( CFG_ERROR_NOSECTION, target->CfgOpen(data_dir + string("cfgOpen_05.txt") ));

	ASSERT_EQ( CFG_ERROR_FORMAT, target->CfgOpen(data_dir + string("cfgOpen_06.txt")));
	ASSERT_EQ( 2, (int)target->GetErrorLine() );

}

TEST_F(CCfgLoadTest, test02)
{
	int ret;

	ret = target->CfgOpen( data_dir + string("YamMonstar.ini") );
	ASSERT_EQ( CFG_PROC_OK, ret );

	ASSERT_EQ( 1, target->GetSecCnt() );
	ASSERT_EQ( 2, target->GetItemCnt( "Multicast" ) );

	string sVal;
	int value;

	ret = target->GetValue( "Multicast", "Port", &value );
	ASSERT_EQ( CFG_PROC_OK, ret );
	ASSERT_EQ( 40001, value );


	ret = target->GetValue( "Multicast", "MulticastAddress", &sVal );
	ASSERT_EQ( CFG_PROC_OK, ret );
	ASSERT_TRUE( sVal.compare("224.5.1.3")==0 );

	ASSERT_EQ(CFG_PROC_OK, target->CfgSave());

}

TEST_F(CCfgLoadTest, test03_Hex)
{
	int ret;
	long lVal;
	unsigned long ulVal;

	ret = target->CfgOpen(data_dir + string("Chart.ini") );
	ASSERT_EQ( CFG_PROC_OK, ret );

	ret = target->GetHexValue( "Common", "UpBorder", &lVal );
	ASSERT_EQ( CFG_PROC_OK, ret );
	ASSERT_EQ( (long)0x00FF0000, lVal );

	ret = target->GetHexValue( "Common", "UpBorder", (long *)&ulVal );
	ASSERT_EQ( CFG_PROC_OK, ret );
	ASSERT_EQ( (unsigned long)0x00FF0000, ulVal );

}

TEST_F(CCfgLoadTest, test04_SetValue)
{
	int ret;
	double val;

	ret = target->CfgOpen(data_dir + string("cfgOpen_01.txt"));
	ASSERT_EQ(CFG_PROC_OK, ret);

	ASSERT_EQ(CFG_PROC_OK, target->SetValue("SEC1", "item1", 2.5));
	ASSERT_EQ(CFG_PROC_OK, target->GetValue("SEC1", "item1", &val));
	ASSERT_EQ(2.5, val);
}

#ifdef _WIN32
TEST_F(CCfgLoadTest, test05_Japanese)
{
	ASSERT_EQ(CFG_PROC_OK, target->CfgOpen(data_dir + string("日本語.txt")));
}
#endif
