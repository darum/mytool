#pragma once

#include "gtest/gtest.h"

#include "CfgLoad.h"

class CCfgLoadTest : public ::testing::Test
{
public:
	CCfgLoadTest();
	~CCfgLoadTest();

	void SetUp();
	void TearDown();

protected:
	// Test Class
	CCfgLoad *target;
};

