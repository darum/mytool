#include "CfgLoad.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/**
 * @brief コンストラクタ
 */
CCfgLoad::CCfgLoad(void)
{
	mErrLine = 0;
}

/**
 * @brief デストラクタ
 */
CCfgLoad::~CCfgLoad(void)
{
}

/**
 * @brief コンフィグファイルを開く
 * @param [in] file ファイル名
 * @return CFG_PROC_OK OK
 * @retunr CFG_ERROR_FOPEN		ファイルオープンエラー
 * @return CFG_ERROR_FORMAT		フォーマットエラー
 * @return CFG_ERROR_NOSECTION	セクションがない
 * @return CFG_ERROR_NOREC		有効なレコードがない
 */
int CCfgLoad::CfgOpen( const string & file )
{
	// File Open
	ifstream fin( file.c_str() );

	if( !fin )
	{
		// File Open Error
		return CFG_ERROR_FOPEN;
	}

	// Set Class Parameters
	mCfgFile = file;

	int ret = mCfgFileRead( fin );
	fin.close();

	return ret;
}

/**
 * @brief コンフィグファイルの保存
 */
int CCfgLoad::CfgSave(void)
{
	int ret;

	ofstream fout(mCfgFile.c_str());

	if(!fout)
	{
		return CFG_ERROR_FOPEN;
	}

	ret = mCfgFileWrite(fout);
	fout.close();

	return ret;
}

#ifndef _WIN32
string CCfgLoad::GetCfgName()
{
	return mCfgFile;
}
#endif
/**
 * @param [out] ret Config File name
 */
int CCfgLoad::GetCfgName(char *ret, const size_t size)
{
	int iRet = CFG_PROC_OK;

	if(this->mCfgFile.size() > size)
	{
		iRet = CFG_ERROR_LENGTH;
	}
	else
	{
#if (defined(_WIN32) && (_MSC_VER >= 1400))
		strcpy_s(ret, size, this->mCfgFile.c_str());
#else
#ifdef STRLCPY
		strlcpy(ret, this->mCfgFile.c_str(), size);
#else
		strncpy(ret, this->mCfgFile.c_str(), size);
#endif
#endif
	}

	return iRet;
}

int CCfgLoad::GetSecCnt()
{
	int ret;
	ret = (int)mData.size();
	return ret;
}

int CCfgLoad::GetItemCnt( const string& item )
{
	int ret;
	map<string, sec_data>::iterator p;

	p = mData.find( item.c_str() );
	if( p == mData.end() )
	{
		// Not found
		return CFG_NOTFOUND_SECTION;
	}

	sec_data section = p->second;

	ret = (int)section.size();
	return ret;
}

int CCfgLoad::GetValue( const string& section, const string& item, int *ret )
{
	string value;
	int retValue;
	
	retValue = mGetValueStr( section, item, &value );
	if( retValue != CFG_PROC_OK ) return retValue;

	*ret = atoi( value.c_str() );
	return CFG_PROC_OK;
}

int CCfgLoad::GetValue( const string& section, const string& item, double *ret )
{
	string value;
	int retValue;

	if(( retValue = mGetValueStr( section, item, &value )) != CFG_PROC_OK )
		return retValue;
	
	*ret = atof( value.c_str() );
	return CFG_PROC_OK;
}

int CCfgLoad::GetValue( const string& section, const string& item, string *ret )
{
	return mGetValueStr( section, item, ret );
}

int CCfgLoad::GetValue( const string& section, const string& item, char *value, const size_t vLen )
{
	string str;
	int ret;

	if(( ret = mGetValueStr( section, item, &str )) != CFG_PROC_OK )
		return ret;
	
#if _MSC_VER >= 1400
	strncpy_s( value, vLen, str.c_str(), vLen );
#else
	strncpy( value, str.c_str(), vLen );
#endif
	return CFG_PROC_OK;
}

int CCfgLoad::GetValue( const string& section, const string & item, long * ret )
{
	string value;
	int retValue;
	
	retValue = mGetValueStr( section, item, &value );
	if( retValue != CFG_PROC_OK ) return retValue;

	*ret = atol( value.c_str() );
	return CFG_PROC_OK;
}

int CCfgLoad::GetHexValue( const string& section, const string & item, long * ret )
{
	string value;
	int retValue;
	
	retValue = mGetValueStr( section, item, &value );
	if( retValue != CFG_PROC_OK ) return retValue;

	*ret = strtol( value.c_str(), NULL, 16 );
	return CFG_PROC_OK;
}

int CCfgLoad::SetValue(const string &section, const string &item, const double val)
{
	map<string, sec_data>::iterator it_sec;
	sec_data::iterator it_item;
	sec_data items;

	if((it_sec = this->mData.find(section)) == this->mData.end())
	{
		return CFG_NOTFOUND_SECTION;
	}

	items = it_sec->second;
	if((it_item = items.find(item)) == items.end())
	{
		return CFG_NOTFOUND_ITEM;
	}

	char c_val[64];
#if defined(_WIN32) && !defined(__MINGW32__)
	sprintf_s(c_val, 64, "%lf", val);
#else
	snprintf(c_val, 64, "%lf", val);
#endif
	return this->mSetValueStr(section, item, string(c_val));

	return CFG_PROC_OK;
}

unsigned int CCfgLoad::GetErrorLine()
{
	unsigned int ret;
	ret = mErrLine;
	mErrLine = 0;		// Initialize
	return ret;
}

///////////////////
// Private
int CCfgLoad::mCfgFileRead( ifstream &fin )
{
	char line[BUF_SIZE];
	bool secFlag = false;
	string str, section;
	unsigned int count = 0;
//	int ret;

	sec_data secData;

	mData.clear();

	while( !fin.eof() )
	{
		memset( line, '\0', BUF_SIZE );
		fin.getline( line, BUF_SIZE );
		count++;
		
		int i;
		i = (int)strlen(line) - 1;
		while( line[i] == ' ' || line[i] == '\r')
		{
			line[i] = '\0';
			i--;
		}

		switch( line[0] )
		{
			// Ignore Line
			case '#':
			case '\n':
			case '\0':
				break;

			// Section
			case '[':
			{
				// Clear buffer
				str.erase(0, str.length());
				str = line;

				string::size_type idx = str.find(']');
				if( idx == string::npos )
				{
					mErrLine = count;
					return CFG_ERROR_FORMAT;
				}

				if( secFlag )
				{
					// 2nd Section ｰﾊｹﾟ
					mData.insert( pair<string, sec_data>(section, secData) );
					secData.clear();
				}

				secFlag = true;
				section = str.substr( 1, idx-1 );
				
				break;
			}
			default:
			{
				string item, value;

				if( !secFlag )
				{
					// Out of the section
					return CFG_ERROR_NOSECTION;
				}

				string::size_type spos;			// Separeted Position
				string strLine = string(line);

				spos = strLine.find("=");
				if( spos != string::npos )
				{
					item = strLine.substr( 0, spos );
					value = strLine.substr( spos+1 );
					if( value.find("=") == string::npos )
					{
						pair <string, string> data( item, value );
						secData.insert( data );
#ifdef DEBUG
						cerr << "Section=[" << section << "] Insert: Item=[" << item << "], Value=[" << value << "]" << endl;
#endif
					}
					else
					{
						// Multiple "="
						mErrLine = count;
						return CFG_ERROR_FORMAT;
					}
				}
				else
				{
					// "=" is not fonud
					mErrLine = count;
					return CFG_ERROR_FORMAT;
				}

				break;
			}
		}
	}

	if( secFlag )
	{
		mData.insert( pair<string, sec_data>(section, secData) );
		secData.clear();
	}

	if( mData.empty() )
	{
		return CFG_ERROR_NOREC;
	}
	else
	{
		return CFG_PROC_OK;
	}
}

int CCfgLoad::mCfgFileWrite(ofstream &fout)
{
	int ret = CFG_PROC_OK;
	map<string, sec_data>::iterator it_sec;
	sec_data::iterator it_item;

	for(it_sec = this->mData.begin(); it_sec != this->mData.end(); ++it_sec)
	{
		if(it_sec != this->mData.begin())
		{
			fout << endl;
		}
		fout << "[" << it_sec->first << "]" << endl;

		sec_data item = it_sec->second;
		for(it_item = item.begin(); it_item != item.end(); ++it_item)
		{
			fout << it_item->first << "=" << it_item->second << endl;
		}
	}

	return ret;
}

int CCfgLoad::mGetValueStr( const string& section, const string& item, string *ret )
{
    map<string, sec_data>::iterator p_sec;
    map<string, string>::iterator p_item;
    sec_data secData;

    // Search Section Name
    p_sec = mData.find( section.c_str() );
    if( p_sec == mData.end() )
    {
        // Not found
        return CFG_NOTFOUND_SECTION;
    }

    secData = p_sec->second;

    // Search Item Name
    p_item = secData.find( item.c_str() );
    if( p_item == secData.end() )
    {
        return CFG_NOTFOUND_ITEM;
    }
    *ret = p_item->second;

    return CFG_PROC_OK;
}

/**
 * @note section, itemは存在していること
 */
int CCfgLoad::mSetValueStr(const string &section, const string &item, const string &val)
{
	// Sectionの取り出し
	map<string, sec_data>::iterator it_sec;
	sec_data newDat;
	it_sec = this->mData.find(section);
	newDat = it_sec->second;
	// 旧データの削除
	this->mData.erase(it_sec);

	// Item群の取り出し
	sec_data::iterator it_data;
	it_data = newDat.find(item);
	// 旧データの削除
	newDat.erase(it_data);

	// 新データの登録（Value）
	newDat.insert(sec_data::value_type(item, val));
	// (Item)
	this->mData.insert(pair<string, sec_data>(section, newDat));

	return CFG_PROC_OK;	
}
