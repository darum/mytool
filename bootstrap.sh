#!/bin/sh
set -ex
if [ ! -d config ] ; then
    mkdir config
fi
if [ ! -d lib ]; then
    mkdir lib
fi
aclocal -I config 
autoheader
automake --foreign --add-missing --copy
autoconf
