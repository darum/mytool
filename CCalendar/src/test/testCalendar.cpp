#include "stdafx.h"

#include "Calendar.h"

#include "testCalendar.h"

	CCalendarTest::CCalendarTest()
	{
	};

	CCalendarTest::~CCalendarTest(){};

	void CCalendarTest::SetUp()
	{
		pCal = new CCalendar();
		hol = new CHoliday();
	};
	void CCalendarTest::TearDown()
	{
		if(pCal)
			delete(pCal);
		if(hol)
			delete(hol);
	};

TEST_F(CCalendarTest, testGetAddDays)
	{
		// toString()
		target = CCalendar::MakeCalDate( 2008, 12, 15 );
		ASSERT_EQ( 20081215L, target);

		// GetAddDays()
		CCalendar result;

		// normal Add
		CCalendar::GetAddDays( target, 1L );
		ASSERT_EQ( 20081216L, target );

		target = CCalendar::MakeCalDate( 2008, 12, 31 );
		CCalendar::GetAddDays( target, 1L );
		ASSERT_TRUE( CCalendar::toString(target).compare("2009/01/01") == 0 );

		target = CCalendar::MakeCalDate( 2008, 2, 28 );
		CCalendar::GetAddDays( target, 1L );
		ASSERT_TRUE( CCalendar::toString(target).compare("2008/02/29") == 0);

		CCalendar::GetAddDays( target, 1L );
		ASSERT_TRUE( CCalendar::toString(target).compare("2008/03/01") == 0);
	};

TEST_F(CCalendarTest,testGetSubDays)
	{
		target = CCalendar::MakeCalDate( 2000, 3, 1 );
		CCalendar::GetSubDays( target, 1L );
		ASSERT_EQ( 20000229L, target );
		CCalendar::GetSubDays( target, 1L );
		ASSERT_EQ( 20000228L, target );

		target = CCalendar::MakeCalDate( 2009, 1, 1 );
		CCalendar::GetSubDays( target, 1L );
		ASSERT_EQ( 20081231L, target );
	};

TEST_F(CCalendarTest, testCHoliday)
	{
		ASSERT_EQ( true,	hol->AddHoliday(20081223));
		ASSERT_EQ( true, hol->Find(20081223) );
		hol->AddHoliday(20081124);
		ASSERT_EQ( true, hol->Find(20081124) );
		ASSERT_EQ( true, hol->DelHoliday(20081124) );
		ASSERT_EQ( false, hol->Find(20081124L) );
	};

TEST_F(CCalendarTest, testWeekDay)
	{
		target = CCalendar::MakeCalDate( 2008, 12, 30 );
		ASSERT_EQ( (unsigned short)2, CCalendar::GetWeekDay(target) );
		CCalendar::GetAddDays( target, 1L );
		ASSERT_EQ( (unsigned short)3, CCalendar::GetWeekDay(target) );
		CCalendar::GetAddDays( target, 1L );
		ASSERT_EQ( (unsigned short)4, CCalendar::GetWeekDay(target) );
	};

TEST_F(CCalendarTest, testCHolidayFind)
	{
		this->hol->AddHoliday( 20081223L );
		ASSERT_EQ( false, this->hol->Find(20081222L) );
		ASSERT_EQ( true, this->hol->Find(20081223L) );
		this->hol->AddHoliday( 20081123L );
		ASSERT_EQ( true, this->hol->Find(20081123L) );
		ASSERT_EQ( true, this->hol->Find(20081223L) );

	};

TEST_F(CCalendarTest, testGetBizDays)
	{
		// �x�ॆआऒ�ॆआ��ॆआ�
		pCal->AddHoliday( 20071103L );
		pCal->AddHoliday( 20071123L );
		pCal->AddHoliday( 20071223L );
		pCal->AddHoliday( 20071231L );
		pCal->AddHoliday( 20080101L );
		pCal->AddHoliday( 20080102L );
		pCal->AddHoliday( 20080103L );
		pCal->AddHolidayMon(1, 2, 2008, 2009 );
		pCal->AddHoliday( 20080211L );

		target = CCalendar::MakeCalDate( 2007, 11, 3 );
		ASSERT_EQ( true, pCal->IsHoliday(target) );
		target = CCalendar::MakeCalDate( 2007, 12, 24 );
		ASSERT_EQ( true, pCal->IsHoliday(target) );
		target = CCalendar::MakeCalDate( 2008, 1, 14 );
		ASSERT_EQ( true, pCal->IsHoliday(target) );
		target = CCalendar::MakeCalDate( 2009, 1, 12 );
		ASSERT_EQ( true, pCal->IsHoliday(target) );


		CALDATE diff;
		target = CCalendar::MakeCalDate( 2007, 12, 17 );
		diff = CCalendar::MakeCalDate( 2007, 12, 28 );
		ASSERT_EQ( 8L, pCal->GetBizDiff( target, diff ) );
		diff = CCalendar::MakeCalDate( 2008, 1, 7 );
		ASSERT_EQ( 10L, pCal->GetBizDiff( target, diff ) );
		diff = CCalendar::MakeCalDate( 2008, 1, 15 );
		ASSERT_EQ( 15L, pCal->GetBizDiff( target, diff ) );
		diff = CCalendar::MakeCalDate( 2008, 2, 29 );
		ASSERT_EQ( 47L, pCal->GetBizDiff( target, diff ) );

		diff = CCalendar::MakeCalDate( 2007, 12, 14 );
		ASSERT_EQ( -1L, pCal->GetBizDiff( target, diff ) );
		diff = CCalendar::MakeCalDate( 2007, 11, 26 );
		ASSERT_EQ( -15L, pCal->GetBizDiff( target, diff ) );
		diff = CCalendar::MakeCalDate( 2007, 11, 4 );
		ASSERT_EQ( -29L, pCal->GetBizDiff( target, diff ) );

	};

TEST_F(CCalendarTest, testGetDiff)
	{
		CALDATE diff;
		target = CCalendar::MakeCalDate(2009, 6, 14);

		diff = CCalendar::MakeCalDate(2009, 6, 15);
		ASSERT_EQ(1L, pCal->GetDiff(target, diff));
		diff = CCalendar::MakeCalDate(2009, 6, 13);
		ASSERT_EQ(-1L, pCal->GetDiff(target, diff));
		diff = CCalendar::MakeCalDate(2009, 6, 8);
		ASSERT_EQ(-6L, pCal->GetDiff(target, diff));
	};

TEST_F(CCalendarTest, testGetBizAddDays)
	{
		target = CCalendar::MakeCalDate(2009, 6, 22);
		this->pCal->GetBizAddDays(target, 1);
		ASSERT_EQ(20090623L, target);
		this->pCal->GetBizAddDays(target, 5);
		ASSERT_EQ(20090630L, target);
	};

TEST_F(CCalendarTest, testGetBizSubDays)
	{
		target = CCalendar::MakeCalDate(2009, 6, 22);
		this->pCal->GetBizSubDays(target, 1);
		ASSERT_EQ(20090619L, target);
		this->pCal->GetBizSubDays(target, 1);
		ASSERT_EQ(20090618L, target);
	};
