// testCCalendarDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "testCCalendar.h"
#include "testCCalendarDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CtestCCalendarDlg ダイアログ




CtestCCalendarDlg::CtestCCalendarDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CtestCCalendarDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CtestCCalendarDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, mEdit);
	DDX_Control(pDX, IDC_BUTTON1, mBtn1);
}

BEGIN_MESSAGE_MAP(CtestCCalendarDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CtestCCalendarDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// CtestCCalendarDlg メッセージ ハンドラ

BOOL CtestCCalendarDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// このダイアログのアイコンを設定します。アプリケーションのメイン ウィンドウがダイアログでない場合、
	//  Framework は、この設定を自動的に行います。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンの設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンの設定

	// TODO: 初期化をここに追加します。

	return TRUE;  // フォーカスをコントロールに設定した場合を除き、TRUE を返します。
}

// ダイアログに最小化ボタンを追加する場合、アイコンを描画するための
//  下のコードが必要です。ドキュメント/ビュー モデルを使う MFC アプリケーションの場合、
//  これは、Framework によって自動的に設定されます。

void CtestCCalendarDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// クライアントの四角形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンの描画
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// ユーザーが最小化したウィンドウをドラッグしているときに表示するカーソルを取得するために、
//  システムがこの関数を呼び出します。
HCURSOR CtestCCalendarDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CtestCCalendarDlg::OnBnClickedButton1()
{
	this->mBtn1.EnableWindow(FALSE);
	
	// イベント・マネージャとテスト・コントローラを生成する
	CPPUNIT_NS::TestResult controller;

	// テスト結果収集リスナをコントローラにアタッチする
	CPPUNIT_NS::TestResultCollector result;
	controller.addListener( &result );

	// 「.」で進行状況を出力するリスナをアタッチする
	CPPUNIT_NS::BriefTestProgressListener progress;
	controller.addListener( &progress );

	// テスト・ランナーにテスト群を与え、テストする
	CPPUNIT_NS::TestRunner runner;
	runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
	runner.run( controller );

	// テスト結果を文字列ストリームに流し入れ、
	std::ostringstream stream;
	stream << "************ Start  TEST ************\n";
	CPPUNIT_NS::CompilerOutputter outputter( &result, stream);
	outputter.write();
	outputter.printStatistics();
	stream << "************ Finish TEST ************\n";
#if 0
	// 得られた文字列をTRACEマクロで出力する
	TRACE(stream.str().c_str());
#else
	std::string strOut = stream.str();
	unsigned long l;
	for( l = 0; l < strOut.size(); ++l )
	{
		if( strOut[l] == '\n' )
		{
			strOut.replace( l, 1, "\r\n" );
			l++;
		}
	}
	this->mEdit.SetWindowText((LPCTSTR)strOut.c_str());
#endif
}
