#pragma warning(disable : 4267)
#include "stdafx.h"			// DLL

#include "Calendar.h"
#include <stdio.h>

#define LEAP(x) (((x)%4 == 0 && (x)%100 != 0 || (x)%400 == 0)?1:0)	// 閏年の判定
#define DMS(yy,mm) (dms[LEAP((yy))][(mm)-1])						// 日数の取得
#define CAL_SET(y,m,d) (long)(y*10000 + m*100 + d)					// 値のセット


static char dms[2][12] = 
{	// 月間の日数テーブル
	{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },		// 平年
	{ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }		// 閏年
};


CCalendar::CCalendar()
{
//	mDateVal = -1;
}

CCalendar::~CCalendar()
{
}


// Public
/**
 * @brief 日付型を取得する
 * @param year 年（西暦）
 * @param month 月
 * @param day 日
 * @return 日付型
 */
CALDATE CCalendar::MakeCalDate(unsigned int year, unsigned int month, unsigned int day)
{
	return CAL_SET( year, month, day );
}

/**
 * @brief 指定日が何日（何営業日）前か取得する（営業日基準）
 * @param base 基準日
 * @param sub 指定日
 * @return ＞0 指定日が前
 * @return ＜0 指定日が後（未来）
 * @exception 日付の指定がおかしい場合にout_of_rangeをthrowする。
 */
//long CCalendar::GetBizDiff( const CCalendar & sub )
long CCalendar::GetBizDiff( const CALDATE base, const CALDATE sub )
{
	long eday=0L; /* 営業日数 */
	CALDATE diff = base;

	if(base < sub)
	{		// 引数が後の日付の場合
		while(diff < sub){
			diff = m_GetAddDay(diff);
			if(!m_IsHoliday(diff, this->mpHoliday)) eday++;
		}
	}
	else if(base > sub){
		while(diff > sub){
			diff = m_GetSubDay(diff);
			if(!m_IsHoliday(diff, this->mpHoliday)) eday--;
		}
	}
	return eday;
}

/** 
 * @brief 加算した日を返す
 * @param base 計算する日付
 * @param add 追加する日数
 */
//void CCalendar::GetAddDays( CCalendar & result, unsigned long add )
void CCalendar::GetAddDays( CALDATE & base, unsigned long add )
{
//	CALDATE res = this->mDateVal;
	while( add )
	{
		base = CCalendar::m_GetAddDay( base );
		add--;
	}

//	result.mDateVal = res;
}

/** 
 * @brief 減算した日を返す
 * @param base 計算する日付
 * @param sub 減算する日数
 */
//void CCalendar::GetSubDays(CCalendar & result, unsigned long sub)
void CCalendar::GetSubDays( CALDATE & base, unsigned long sub )
{
	while( sub )
	{
		base = CCalendar::m_GetSubDay( base );
		sub--;
	}
}

/** 
 * @brief 曜日を返す
 * @return 0..6 曜日（0=日、・・・)

 */
unsigned short CCalendar::GetWeekDay(CALDATE base)
{
	return CCalendar::m_GetWeekDay(base);
}

/** 
 * @brief 休日テーブルに追加する
 * @param hol 設定する休日
 * @param adjust すでに休日（日曜または祝日設定済み）だった場合に調整するか。trueで、翌平日を祝日として登録。
 * @note adjust=true の場合、2007年以前は、月曜日のみ祝日設定。2008年以降は、翌平日を祝日とする。（月曜日もすでに祝日の場合は、火曜日に設定される）
 * @return true OK
 */
bool CCalendar::AddHoliday(CALDATE hol, bool adjust )
{
	bool ret = true;

	if( adjust )
	{
		if( this->m_GetWeekDay(hol) == 0 )
		{		// 日曜
			hol = m_GetAddDay(hol);
		}
		if( hol > 20080101 )
		{	// 2008年以降は、翌平日を休日とする
			while( this->mpHoliday.Find(hol) )
			{		// 祝日設定済みの場合
				hol = this->m_GetAddDay(hol);
			}
		}
	}

	return this->mpHoliday.AddHoliday(hol);
}

/** 
 * @brief ハッピーマンデーを設定する
 * @param month 月
 * @param week 第何週か
 * @param sYear 設定を開始する年
 * @param eYear 設定を終了する年。0か、不正な値の場合、sYearの年だけ設定。
 * @return 0 設定完了
 * @return 0> 設定済みの年を返す。その年〜eYearの間は設定されない。
 */
unsigned int CCalendar::AddHolidayMon( unsigned int month, unsigned int week, unsigned int sYear, unsigned int eYear )
{
	unsigned int ret = 0;
	CALDATE setHol;

	if( eYear == 0 || eYear < sYear )
	{
		eYear = sYear;		// 1年だけ設定
	}

	unsigned int i;
	for( i = sYear; i <= eYear; ++i )
	{
		unsigned short wd;
		setHol = CAL_SET(i, month, 1);
		wd = m_GetWeekDay(setHol);
		CCalendar::GetAddDays( setHol, (long)((8 - wd) % 7 + (week - 1) * 7));
		if( this->m_IsHoliday(setHol, this->mpHoliday) == true )
		{		// すでに設定済みの場合
			ret = i;
			break;
		}
		this->mpHoliday.AddHoliday(setHol);
	}

	return ret;
}

/** 
 * @brief 日付文字列の取得（yyyy/mm/dd）
 * @return 日付文字列
 */
string CCalendar::toString(CALDATE date)
{
	string ret;
	char chStr[16];

#if (defined(_MSC_VER) && (_MSC_VER >= 1400))
	_snprintf_s( chStr, 16, 16, "%04d/%02d/%02d", date/10000, (date/100)%100, date % 100 );
#else
	snprintf( chStr, 16, "%04d/%02d/%02d", (int)date/10000, (int)((date/100)%100), (int)(date % 100) );
#endif

	ret = string(chStr);

	return ret;

}

/**
 * @brief 日付の差を取得する
 * @param [in] base 基準となる日
 * @param [in] diff 差を求める日
 * @return ＞0 指定日が前
 * @return ＜0 指定日が後（未来）
 */
long CCalendar::GetDiff(const CALDATE base, const CALDATE sub)
{
	long eday=0L; /* 日数 */
	CALDATE diff = base;

	if(base < sub)
	{		// 引数が後の日付の場合
		while(diff < sub){
			diff = m_GetAddDay(diff);
			eday++;
		}
	}
	else if(base > sub){
		while(diff > sub){
			diff = m_GetSubDay(diff);
			eday--;
		}
	}
	return eday;
}

/** 
 * @brief 加算した日を返す（営業日基準）
 * @param base 計算する日付
 * @param add 追加する日数
 */
void CCalendar::GetBizAddDays( CALDATE & base, unsigned long add )
{
	while( add )
	{
		base = CCalendar::m_GetAddDay( base );
		if(CCalendar::m_IsHoliday(base, this->mpHoliday) == false)
		{
			add--;
		}
	}
}

/** 
 * @brief 減算した日を返す（営業日基準）
 * @param base 計算する日付
 * @param sub 減算する日数
 */
void CCalendar::GetBizSubDays( CALDATE & base, unsigned long sub )
{
	while( sub )
	{
		base = CCalendar::m_GetSubDay( base );
		if(CCalendar::m_IsHoliday(base, this->mpHoliday) == false)
		{
			sub--;
		}
	}
}

///////////////////////////////////////////////////////////////////////
// Protected / Private
/** 
 * @brief 1日加算した日を取得する
 * @param base 基準となる日
 * @return 基準となる日の翌日
 */
CALDATE CCalendar::m_GetAddDay( const CALDATE & base )
{
	int yy,mm,dd;

	yy = base/10000;
	mm = (base/100)%100;
	dd = base%100;

	/* 月末でなければ単純に1を足す */
	if(dd < DMS(yy,mm)) 
		return base+1;

	/* 年末でなければ次の月の1日 */
	if(++mm<=12)  
		return (yy*100L+mm)*100+1;

	/* 年末だったら次の年の1月1日 */
	return ((yy+1)*100L + 1)*100+1;
}

/**
 * @brief 1日減算した日を取得する
 * @param [in] base 基準となる日
 * @return 基準となる日の前日
 */
CALDATE CCalendar::m_GetSubDay( const CALDATE & base )
{
	int yy,mm,dd;
	/* 月初でなければ単純に1を引く */
	dd=base%100;
	if(dd!=1) return base-1;

	/* 年初でなければ前月末 */
	yy=base/10000;
	mm=(base/100)%100;
	if(--mm>0) return (yy*100L+mm)*100+DMS(yy,mm);

	/* 年初ならば前年の12月31日 */
	return ((yy-1)*100L+12)*100+31;
}

/** 
 * @brief 休日かどうかを判定する
 * @return true 休日
 * @return false 平日
 */
bool CCalendar::IsHoliday(CALDATE date)
{
	return m_IsHoliday( date, this->mpHoliday );
}

/**
 * @brief 有効な日付か判定する
 * @param [in] target 日付
 * @return true 有効な日付
 * @return false 無効な日付
 */
bool CCalendar::m_IsValidDate( const CALDATE & target )
{
	bool ret = false;
	int yy,mm,dd;

	yy = target/10000;
	mm = (target/100)%100;
	dd = target%100;
	while(true)
	{
		if(target<START || target>END)
			break;
		if(mm<1    || mm>  12)
			break;
		if(dd<1 || dd>DMS(yy,mm))
			break;

		ret = true;
		break;
	}

	return ret;
}

/**
 * @brief 曜日の取得
 * @param [in] target 日付
 * @return 曜日（0=日, 1=月, ..., 6=土）
 */
unsigned short CCalendar::m_GetWeekDay(const CALDATE &target)
{
	int yy,mm,dd,w;
	mm=(target/100)%100;
	dd=target%100;
	yy = (target/10000);

	if(mm < 3){
		yy--;
		mm += 12;
	}

	w = (5 * (yy)/4 - yy/100 + yy/400 + (int)(2.6 * mm + 1.6) + dd) % 7;
	return w;
}

/**
 * @brief 休日か判定する
 * @param [in] target 日付
 * @param [in] hol 休日情報
 * @return true 休日
 * @return false 平日
 */
bool CCalendar::m_IsHoliday( const CALDATE & target, CHoliday & hol )
{
	bool ret = false;
	unsigned short int wd;

	wd = m_GetWeekDay( target );
	if( wd == 0 )
	{	// 日曜
		ret = true;
	}
	else if( wd == 6 && target > 19900101 )
	{	// 1990年以降の6月
		ret = true;
	}

	if( ret == false )
	{
		ret = hol.Find(target);
	}

	return ret;	

}

///////////////////////////////////////////////////////////////////////
// CHoliday
///////////////////////////////////////////////////////////////////////
/**
 * @brief コンストラクタ
 */
CHoliday::CHoliday()
{
	this->m_tbl.clear();
}

/** 
 * @brief 休日を追加する
 * @param hol 追加する休日
 * @param ignSun 日曜を無視する。falseの場合、翌日が休日となる。
 * @return true 成功（すでに存在していても成功となる）
 * @return false 失敗
 */
bool CHoliday::AddHoliday(CALDATE hol)
{
	bool ret = true;

	long low = 0, high = (long)this->m_tbl.size();
	vector<CALDATE>::iterator i = this->m_tbl.begin();

	if( this->m_tbl.size() == 0 )
	{
		this->m_tbl.push_back(hol);
		return ret;
	}
	while(true)
	{
		i = this->m_tbl.begin() + (high + low)/2;		// 探索ポイント
		if( *i == hol )
		{		// すでに登録済みの場合
			break;
		}
		else
		{
			if( *i < hol )
			{
				if( (i+1) == m_tbl.end() )
				{
					this->m_tbl.push_back(hol);
					break;
				}
				else if( *(i+1) > hol )
				{		// [i]〜[i+1]の間にいる場合
					this->m_tbl.insert(i+1, hol);		// 登録
					break;
				}
				else
				{		// [i]より大きい場合
					low = (long)(i - this->m_tbl.begin());		// iを探索点下限
				}
			}
			else
			{	// [i]より小さい場合
				if( i == m_tbl.begin() )
				{
					this->m_tbl.insert(i, hol );
					break;
				}
				else if( *(i-1) < hol )
				{
					this->m_tbl.insert( i, hol );
					break;
				}
				high = (long)(i - this->m_tbl.begin());			// iを探索点上限
			}
		}
	}

	return ret;
}

/** 
 * @brief 休日を削除する
 * @param hol 削除する日付
 * @return true 削除済み
 * @return false 登録されてない
 */
bool CHoliday::DelHoliday(CALDATE hol)
{
	bool ret = true;
	int pos;

	if((pos = m_Find(hol)) == -1 )
	{
		ret = false;
	}
	else
	{
		this->m_tbl.erase(this->m_tbl.begin() + pos);
	}
	return ret;
}

/**
 * @brief 休日テーブルを検索する
 * @param [in] target 検索する日
 * @return true 登録済み
 * @return false 未登録
 */
bool CHoliday::Find(CALDATE target)
{
	bool ret = false ;

	if( this->m_Find(target) != -1 )
	{
		ret = true;
	}

	return ret;
}

/**
 * @brief 休日テーブルの検索(実体)
 * @param [in] target 検索する日
 * @return -1 未登録
 * @return >0 見つかった位置
 */
int CHoliday::m_Find(const CALDATE target) 
{
	int ret = -1;
	long low = 0, high = this->m_tbl.size(), i;

	if(high != 0)
	{
		i = (low + high)/2;
		while( (m_tbl[i] != target) && (low != high))
		{
			if( m_tbl[i] > target )
			{
				if( high == i )
				{
					break;
				}
				high = i;
			}else{
				if( low == i )
				{
					break;
				}
				low = i;
			}
			i = (low + high)/2;
		}

		if( m_tbl[i] == target )
		{
			ret = i;
		}
	}

	return ret;
}
