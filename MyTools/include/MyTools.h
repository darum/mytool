// DLLへ出力するライブラリ
//#include "Sock.h"
#include "Calendar.h"
#include "MyThread.h"


// 以下の ifdef ブロックは DLL からのエクスポートを容易にするマクロを作成するための 
// 一般的な方法です。この DLL 内のすべてのファイルは、コマンド ラインで定義された MYTOOLS_EXPORTS
// シンボルでコンパイルされます。このシンボルは、この DLL を使うプロジェクトで定義することはできません。
// ソースファイルがこのファイルを含んでいる他のプロジェクトは、 
// MYTOOLS_API 関数を DLL からインポートされたと見なすのに対し、この DLL は、このマクロで定義された
// シンボルをエクスポートされたと見なします。
#ifdef MYTOOLS_EXPORTS
#define MYTOOLS_API __declspec(dllexport)
#else
#define MYTOOLS_API __declspec(dllimport)
#endif

// このクラスは MyTools.dll からエクスポートされました。
class MYTOOLS_API CMyTools {
public:
	CMyTools(void);
	// TODO: メソッドをここに追加してください。
};

extern MYTOOLS_API int nMyTools;

MYTOOLS_API int fnMyTools(void);
