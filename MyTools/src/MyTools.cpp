// MyTools.cpp : DLL アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"
#include "MyTools.h"


#ifdef _MANAGED
#pragma managed(push, off)
#endif

//#pragma comment(lib, "CSock.lib")
//#pragma comment(lib, "CMyThread.lib")
//#pragma comment(lib, "CCalendar.lib")

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
    return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

// これは、エクスポートされた変数の例です。
MYTOOLS_API int nMyTools=0;

// これは、エクスポートされた関数の例です。
MYTOOLS_API int fnMyTools(void)
{
	return 42;
}

// これは、エクスポートされたクラスのコンストラクタです。
// クラス定義に関しては MyTools.h を参照してください。
CMyTools::CMyTools()
{
	return;
}
