#ifdef __MINGW32__
#include "config.h"
#endif

#include "HttpCom.h"
#include "HttpCom_priv.h"

#include <sstream>

static const char *chHeader[] = {"Content-Type", "Host", "Location", "Content-Length"};

CHttpCom::CHttpCom(const CHttpMsg::HTTPVer ver)
:m_httpVer(ver)
{
	// Optionの初期値設定
	this->mSetDefaultOption();

	// HTTPMessageの内容初期化
	this->m_httpSendMsg.ClearAll();
	this->m_httpRecvMsg.ClearAll();

	this->m_pSock = NULL;
#ifdef HAVE_SSL
	this->m_pSsl = NULL;
#endif

	this->m_lastSendMsg.clear();
	this->m_lastRecvMsg.clear();

	this->mInit();

}

CHttpCom::~CHttpCom(void)
{
}

///////////////////////////////////////
// 通信系メソッド
/** サーバへの接続
 * @param [in] server 接続するサーバ名
 * @param [in] port 接続ポート
 * @param [in] proto プロトコル
 * @return HTTPCOM_RETURN_OK OK
 * @return HTTPCOM_ALREADY_OPEN すでに接続済み
 */
int CHttpCom::ConnectServer(const std::string *server, const unsigned short port, const CHttpCom::Protocol proto)
{
	int ret = HTTPCOM_RETURN_OK;

	this->m_proto = proto;

	if(mIsActive())
	{
		return HTTPCOM_ALREADY_OPEN;
	}

	switch(this->m_proto)
	{
	case CHttpCom::HTTP:
		this->m_pSock = new CSock();

		// 接続
		if(this->m_pSock->OpenCli(AF_INET, SOCK_STREAM, server->c_str(), port) == false)
		{
			ret = this->m_pSock->GetLastError();
		}

		break;

#ifdef HAVE_SSL
	case CHttpCom::HTTPS:
		this->m_pSsl = new CSslSock();

		if(this->m_pSsl->OpenCli(AF_INET, SOCK_STREAM, server->c_str(), port) == false)
			ret = this->m_pSsl->GetLastError();

		break;
#endif
		
	default:
		// Error
		break;
	}

	if(ret == HTTPCOM_RETURN_OK)
	{
		// 送信バッファのクリア
		this->m_httpSendMsg.ClearAll();
	}

	return ret;
}

/** 接続のクローズ
 * 
 */
int CHttpCom::Close()
{
	int ret = HTTPCOM_RETURN_OK;

	switch(this->m_proto)
	{
	case CHttpCom::HTTP:
		this->m_pSock->Close();
		delete(this->m_pSock);
		this->m_pSock = NULL;
		break;

#ifdef HAVE_SSL
	case CHttpCom::HTTPS:
		this->m_pSsl->Close();
		delete(this->m_pSsl);
		this->m_pSsl = NULL;
		break;
#endif
		
	default:
		break;
	}

	return ret;
}

/** サーバへリクエストを送信する
 * この関数までに指定したパラメータ／オプションで送信する
 * @param [in] page リクエストページ
 * @return HTTPCOM_RETURN_OK OK
 */
int CHttpCom::SendRequest(const std::string *page, HttpMethod method)
{
	int ret = HTTPCOM_RETURN_OK;
	string msg, buf;
	stringstream ss_opt;
	bool rc;

	if(!mIsActive())
	{
		ret = HTTPCOM_NOT_CONNECTED;
	}

#if 0
	if(ret == HTTPCOM_RETURN_OK)
	{
		if(this->m_httpVer == CHttpMsg::HTTP_VER_1_1)
		{
			// ホスト名の設定（HTTP 1.1 必須）
			char host[256];
	//		char domain[256];
			if(this->m_fSendHost == true)
			{
				gethostname(host, 256);
				ret = this->m_httpSendMsg.SetHeader("Host", string(host));
			}
		}
	}
#endif

	if(ret == HTTPCOM_RETURN_OK)
	{
		// 必須ヘッダのチェック
		ret = this->m_httpSendMsg.CheckHeader(this->m_httpVer);
	}

	if(ret == HTTPCOM_RETURN_OK)
	{
		// メッセージの取得
		ret = this->m_httpSendMsg.GetHttpMsg(&buf);
	}

	if(ret == HTTPCOM_RETURN_OK)
	{		// オプションからヘッダの生成
		ss_opt.str("");


	}

	if(ret == HTTPCOM_RETURN_OK)
	{
		// メッセージ組み立て
		msg.clear();
		msg = mGetRequestString(page, method) + ss_opt.str() + buf + "\r\n\r\n";

		// 送信
		switch(this->m_proto)
		{
		case CHttpCom::HTTP:
			rc = this->m_pSock->Send(msg.c_str(), msg.size());
			if(rc == false)	ret = this->m_pSock->GetLastError();
			break;

#ifdef HAVE_SSL
		case CHttpCom::HTTPS:
			rc = this->m_pSsl->Send(msg.c_str(), msg.size());
			if(rc == false)	ret = this->m_pSsl->GetLastError();
			break;
#endif
		}

	}

	if(ret == HTTPCOM_RETURN_OK)
	{
		// メッセージを保持
		this->m_lastSendMsg = msg;

		// 受信バッファのクリア(送信が完了したから)
		this->m_httpRecvMsg.ClearAll();
	}

	return ret;
}

/** サーバからレスポンスを受信する
 * 受信結果はクラスで保持する
 * @param [out] code HTTPレスポンスコード
 * @return HTTPCOM_RETURN_OK OK
 * @return >0 WinSock Error
 */
int CHttpCom::GetResponse(unsigned short *code)
{
	int ret = HTTPCOM_RETURN_OK;
	string line;
	string header;

	this->m_lastRecvMsg.clear();

#if 1
	// ステータス行の受信
	ret = this->mRecvLine(line);
	if(ret == HTTPCOM_RETURN_OK)
	{
		this->m_lastRecvMsg += line;
		ret = this->m_httpRecvMsg.ParseStatusLine(&line);
	}

	// ヘッダ部の受信
	if(ret == HTTPCOM_RETURN_OK)
	{
		while(true){
			ret = this->mRecvLine(line);
			if(ret != HTTPCOM_RETURN_OK)
			{
				break;
			}

			this->m_lastRecvMsg += line;
			if(line.compare("\r\n") == 0)
			{					// ヘッダ部終了
				break;
			}

			ret = this->m_httpRecvMsg.ParseHeaderLine(&line);
			if(ret != HTTPCOM_RETURN_OK)
				break;
		}
	}

	if(ret == HTTPCOM_RETURN_OK)
	{
		int body_size;
		string body;

		if((body_size = this->m_httpRecvMsg.GetContentLen()) != -1)
		{
			// Bodyを受信する
			ret = this->mRecvBody((size_t)body_size, body);
			this->m_lastRecvMsg += body;
			this->m_httpRecvMsg.SetBody(&body);
			if(body_size != (int)body.size())
			{
				ret = HTTPCOM_MSGRECV_INCOMPLETE;
			}
		}
		else
		{ // 長さが不定
			body = "";
			while(true)
			{
				ret = this->mRecvLine(line);
				if(ret != HTTPCOM_RETURN_OK)
					break;

				this->m_lastRecvMsg += line;
				if(line.compare("\r\n") == 0)
				{
					this->m_httpRecvMsg.SetBody(&body);
					break; // Body受信終了
				}

				body += line;
			}
		}
	}
#else
	ret = this->mHttpRecv(m_lastRecvMsg);
	MyOutputDebugString(_T("mHttpRecv ret=%X\n"), ret);

	if(ret == HTTPCOM_RETURN_OK)
	{
		ret = this->m_httpRecvMsg.ParseMessage(&m_lastRecvMsg);
		MyOutputDebugString(_T("ParseMessage ret=%d\n"), ret);
	}
#endif

	if(ret == HTTPCOM_RETURN_OK)
	{
		// ステータスの取得
		HttpMsgInfo info;
		ret = this->m_httpRecvMsg.GetStatusInfo(&info);
		*code = info.status;

		// 送信Bodyのクリア
		this->m_httpSendMsg.ClearBody();
	}

	return ret;
}

int CHttpCom::DumpResponseMsg(std::string *msg)
{
	int ret = HTTPCOM_RETURN_OK;

	*msg = this->m_lastRecvMsg;

	return ret;
}

int CHttpCom::GetHttpHeader(const Header item, string *val)
{
	return this->m_httpRecvMsg.GetHeader(chHeader[item], val);
}

int CHttpCom::GetResBody(string * html)
{
	return this->m_httpRecvMsg.GetBody(html);
}

///////////////////////////////////////
// 送信メッセージ組み立て
/** 送信メッセージのクリア
 *
 */
int CHttpCom::SendMsgInit()
{
	this->m_httpSendMsg.ClearAll();

	return HTTPCOM_RETURN_OK;
}

/** 送信ヘッダのセット
 *
 */
int CHttpCom::SetHttpHeader(const CHttpCom::Header item, const string *val)
{
	return this->m_httpSendMsg.SetHeader(this->m_cSHeader[item], *val);
}

/** 送信メッセージのセット
 *
 */
int CHttpCom::SetBody(const std::string *body)
{
	int ret = HTTPCOM_RETURN_OK;

	ret = this->m_httpSendMsg.SetBody(body);

	return ret;
}

/** オプションのセット
 * @param [in] item 設定するアイテム
 * @param [in] val 設定する値
 * @return HTTPCOM_RETURN_OK OK
 * @return HTTPCOM_OPTION_UNSUPPORTED 設定するItemは非サポート
 */
int CHttpCom::SetOption(const CHttpCom::Option item, const std::string *val)
{
	int ret = HTTPCOM_RETURN_OK;

	switch(item)
	{
#if 0
	case CHttpCom::OPT_HTTP_VER:					// HTTP Version
		this->m_httpVer = *val;
		break;
#endif
	default:
		ret = HTTPCOM_OPTION_UNSUPPORTED;
		break;
	}

	return ret;
}

/** オプションのセット
 * @param [in] item 設定するアイテム
 * @param [in] flag 設定する値(ON/OFF)
 * @return HTTPCOM_RETURN_OK OK
 * @return HTTPCOM_OPTION_UNSUPPORTED 設定するItemは非サポート
 */
int CHttpCom::SetOption(const CHttpCom::Option item, bool flag)
{
	int ret = HTTPCOM_RETURN_OK;

	switch(item)
	{
#if 0
	case CHttpCom::OPT_HOSTNAME_SEND:
		this->m_fSendHost = flag;
		break;
#endif
	case CHttpCom::OPT_COOKIE_SEND:
		this->m_fCookieCont = flag;
		break;
	default:
		break;
	}

	return ret;
}

/** 最後の送信したメッセージの取得
 *
 */
int CHttpCom::DumpRequestMsg(std::string *msg)
{
	int ret = HTTPCOM_RETURN_OK;

	*msg = this->m_lastSendMsg;

	return ret;
}

/** パケットオブジェクトが初期化されているかチェックする
 * @return true 初期化済み
 * @return false 初期化してない
 */
bool CHttpCom::mIsActive(void)
{
	bool ret = false;
	
	if(this->m_pSock != NULL)
	{
		ret = true;
	}
#ifdef HAVE_SSL
	else if(this->m_pSsl != NULL)
	{
		ret = true;
	}
#endif
	
	return ret;
}

// Static Method
/** URLを分割する
 * @param url [in] 分割元のURL
 * @param proto [out] プロトコル. HTTP/HTTPS以外だとエラーとなる
 * @param server [out] サーバ名
 * @param port [out] ポート番号。指定が無い場合は0。
 * @param page [out] ページアドレス。
 */
int CHttpCom::ParseUrlString
	(const string * url, Protocol *proto, string * server, unsigned short *port, string *page)
{
	int ret = HTTPCOM_RETURN_OK;

	size_t pos, st = 0;

	if((pos = url->find("://")) == string::npos)
	{
		// 見つからない
		ret = HTTPCOM_INVALID_URL;
	}

	if(ret == HTTPCOM_RETURN_OK)
	{
		// プロトコルの取得
		string tmp = url->substr(st, pos - st);
		if(tmp.compare("http") == 0)
		{
			*proto = CHttpCom::HTTP;
		}
#ifdef HAVE_SSL
		else if(tmp.compare("https") == 0)
		{
			*proto = CHttpCom::HTTPS;
		}
#endif
		else
		{
			ret = HTTPCOM_INVALID_URL;
		}
	}

	st = pos + 3;
	if(ret == HTTPCOM_RETURN_OK)
	{
		// serverの取得
		size_t colon, slash;
		colon = url->find(":", st);
		slash = url->find("/", st);

		if(colon == string::npos)
		{
			// portがない
			*port = 0;
			if(slash == string::npos)
			{
				// page指定なし
				*page = string("/");
				*server = url->substr(st);
			}
			else
			{
				*server = url->substr(st, slash - st);
				*page = url->substr(slash);
			}
		}
		else
		{
			if(slash == string::npos)
			{
				// port指定あり
				// pageなし
				*server = url->substr(st, colon - st);
				*port = atoi(url->substr(colon + 1).c_str());
				*page = string("/");
			}
			else
			{
				// pageあり
				if(colon > slash)
				{
					// portなし
					*server = url->substr(st, slash - 1 - st);
					*port = 0;
					*page = url->substr(slash);
				}
				else
				{
					*server = url->substr(st, colon - st);
					*port = atoi(url->substr(colon + 1, slash - st).c_str());
					*page = url->substr(slash);
				}
			}
		}

	}

	return ret;
}


