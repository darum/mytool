// 事前にHttpCom.hをincludeしておくこと
#ifndef _HTTPCOM_PRIV_H
#define _HTTPCOM_PRIV_H

#include "dbg_func/DebugOut.h"

#ifdef _WIN32
#define _MYTIME_TYPE DWORD
#define SLEEP(a) Sleep((a))
#else
#include <unistd.h>
#define _MYTIME_TYPE unsigned long
#define SLEEP(a) usleep(a)
#endif

#endif
