#include "HttpMsg.h"
#include "HttpCom.h"
#include "HttpCom_priv.h"
#include <sstream>

const char *REQUIRED_HEADER_1_1[] = {
	"Host",
};

CHttpMsg::CHttpMsg(void)
{
	ClearAll();
}

CHttpMsg::~CHttpMsg(void)
{
}

/** 内部データをクリアする 
 * 
 */
int CHttpMsg::ClearAll(void)
{
	int ret = HTTPCOM_RETURN_OK;

	this->m_header.clear();
	this->m_cookie.clear();
	this->m_body.clear();

	return ret;
}

int CHttpMsg::ClearBody(void)
{
	int ret = HTTPCOM_RETURN_OK;

	this->m_body.clear();

	return ret;
}

/** 送信用ヘッダーの設定
 * @note 送信メッセージ取得やクッキー設定の前にこの関数を呼ぶ. この関数によって、クラス内部情報を初期化する.
 */
int CHttpMsg::SetHeader(const std::string item, const std::string value)
{
	this->m_header.insert(pair<string, string>(item, value));

	return HTTPCOM_RETURN_OK;
}

/** 送信用メッセージの設定
 *
 */
int CHttpMsg::SetBody(const std::string *body)
{
	int ret = HTTPCOM_RETURN_OK;

	this->m_body = *body;

	return ret;
}

int CHttpMsg::CheckHeader(CHttpMsg::HTTPVer & ver)
{
	int ret = HTTPCOM_RETURN_OK;
	map<string, string>::iterator it;
	unsigned int i;

	if(ver == CHttpMsg::HTTP_VER_1_1)
	{
	  	for(i = 0; i < (sizeof(REQUIRED_HEADER_1_1)/sizeof(char *)); ++i)
		{
			if(this->m_header.find(REQUIRED_HEADER_1_1[i]) == this->m_header.end())
			{
				ret = HTTPCOM_SHORTAGE_REQ_HEADER;
				break;
			}
		}
	}

	return ret;
}

/** 設定した情報から、送信するHTTPメッセージを取得する（ヘッダとBody）
 * @return HTTPCOM_SHORTAGE_REQ_HEADER 必須ヘッダが不足
 */
int CHttpMsg::GetHttpMsg(std::string *msg, bool secure)
{
	int ret = HTTPCOM_RETURN_OK;
	map<string, string>::iterator it_h;
	vector<CookieInfo>::iterator it;
	stringstream ss;

	// ヘッダの設定
	ss.str("");
	for(it_h = this->m_header.begin(); it_h != this->m_header.end(); ++it_h)
	{
		ss << it_h->first << ": " << it_h->second << "\r\n";
	}

	if(this->m_cookie.size() != 0)
	{
		// Cookieのセット
		ss << "Cookie: ";
		for(it = this->m_cookie.begin(); it != this->m_cookie.end(); ++it)
		{
			// セキュアの判定
			if((it->isSecure == true) && (secure == false))
			{
				continue;
			}

			// ドメインの判定
			// ToDo: ドメイン判定の実装

			// パスの判定
			// ToDo: パス判定の実装

			// 有効期限の判定
			if(it->expire != 0)
			{
				// 有効期限が設定されている
				// ToDo: 有効期限処理の実装
			}

			// メッセージへ追加処理
			ss << it->data << "\r\n";
		}
//		ss << "\r\n";
	}

	size_t con_len = this->m_body.size();
	if(con_len != 0)
	{
		ss << "Content-Length: " << con_len << "\r\n";
		ss << "\r\n";
		ss << this->m_body;
	}
	else
	{
		ss << "\r\n";
	}

	*msg = ss.str();

	return ret;
}

int CHttpMsg::ParseHeaderLine(const string *msg)
{
	int ret = HTTPCOM_RETURN_OK;

	if(msg->size() == 0)
	{
		ret = HTTPCOM_HTTPMSG_INVALID;
	}

	if(ret == HTTPCOM_RETURN_OK)
	{
		size_t pos, deli;
		string item, val;

		pos = msg->find("\r\n");
		if(pos != msg->size() - 2)
		{
			// 行末以外に\r\nがある
			ret = HTTPCOM_HTTPMSG_INVALID;
		}

		deli = msg->find(":");
		item = msg->substr(0, deli);
		val = msg->substr(deli + 1, pos - deli - 1);	// 改行分を減らす

		// Spaceの削除
		size_t i;
		size_t _trim, _trlen;
		i = 0;
		while(item[i] == ' ')	i++;
		_trim = i;
		i = item.size();
		while(item[i] == ' ')	i--;
		_trlen = i - _trim;
		item = item.substr(_trim, _trlen);

		i = 0;
		while(val[i] == ' ')	i++;
		_trim = i;
		i = val.size();
		while(val[i] == ' ')	i--;
		_trlen = i - _trim;
		val = val.substr(_trim, _trlen);

		if(item.compare("Set-Cookie") == 0)
		{				// Cookieの場合
			ret = mParseCookieData(val);
		}
		else
		{				// その他のヘッダ
			this->m_header.insert(pair<string, string>(item, val));
		}
	}		

	return ret;
}

int CHttpMsg::ParseStatusLine(const string *line)
{
	size_t pos, st;
	string buf;

	if((pos = line->find(" ")) == string::npos)
	{
		return HTTPCOM_HTTPMSG_INVALID;
	}
	this->m_replyInfo.httpVer = line->substr(0, pos);
	st = pos + 1;

	if((pos = line->find(" ", st)) == string::npos)
	{
		return HTTPCOM_HTTPMSG_INVALID;
	}
	buf = line->substr(st, pos - st);
	this->m_replyInfo.status = atoi(buf.c_str());
	st = pos + 1;

	this->m_replyInfo.strStatus = line->substr(st);

	return HTTPCOM_RETURN_OK;
}


/** 最後の受信したメッセージのStatus Lineの情報を返す
 * @note 1度も受信せずにこの関数を呼んだ場合は、返される情報は不定
 */
int CHttpMsg::GetStatusInfo(HttpMsgInfo *pInfo)
{
	int ret = HTTPCOM_RETURN_OK;

	*pInfo = this->m_replyInfo;

	return ret;
}

/** Content-Length の値を取得
 * @return >=0 Content-Lengthの値
 * @return -1 ヘッダが存在しない
 */
int CHttpMsg::GetContentLen(void)
{
	int ret = -1;

	map<string, string>::iterator it;
	if((it = this->m_header.find("Content-Length")) != this->m_header.end())
	{
		ret = atoi(it->second.c_str());
	}

	return ret;
}

/** 
 *
 */
int CHttpMsg::GetHeader(const std::string item, std::string *val)
{
	int ret = HTTPCOM_RETURN_OK;
	map<string, string>::iterator it;

	if((it = this->m_header.find(item)) != this->m_header.end())
	{
		*val = it->second;
	}
	else
	{
		ret = HTTPCOM_HEADER_NOT_FOUND;
	}

	return ret;
}

int CHttpMsg::GetBody(string *msg)
{
	*msg = this->m_body;

	return HTTPCOM_RETURN_OK;
}

////////////////////////////////////////////////////////////////////////
// Protected Methos
////////////////////////////////////////////////////////////////////////
int CHttpMsg::mParseCookieData(std::string &cookie)
{
	int ret = HTTPCOM_RETURN_OK;
	size_t pos, st;
	string buff;
	CookieInfo addDat;

	// Data
	pos = cookie.find(";");
	buff = cookie.substr(0, pos - 1);
	addDat.data = buff;	
	st = pos + 1;

	while((pos = cookie.find(";", st)) != string::npos)
	{
		buff = cookie.substr(st, pos - 1);

		// 先頭の空白の削除
		size_t frt = buff.find_first_not_of(" ");
		if(frt != string::npos)
		{
			buff.erase(0, frt - 1);
		}

		if(buff.compare("secure") == 0)
		{
			addDat.isSecure = true;
		}
		else
		{
			string item, val;
			size_t deli = buff.find("=");
			if(deli != string::npos)
			{
				// Cookie データにセパレータ'='がない
				ret = HTTPCOM_HTTPMSG_INVALID;
				break;
			}
			item = buff.substr(0, deli -1);
			val = buff.substr(deli + 1);

			if(item.compare("expires") == 0)
			{
				// ToDo: 有効期限の保持
				// Wdy, DD-Mon-YYYY HH:MM:SS GMT

			}
			else if(item.compare("path") == 0)
			{
				addDat.path = val;
			}
			else if(item.compare("domain") == 0)
			{
				addDat.domain = val;
			}
		}
	}

	return ret;
}
