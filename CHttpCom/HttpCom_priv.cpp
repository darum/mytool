#include <sstream>

#include "HttpCom.h"
#include "HttpCom_priv.h"

#define HTTPCOM_PRIV_RECV_UNIT 1024
static struct timeval _com_to = {0, 0};
const  _MYTIME_TYPE retry_time = 60;			// 待ち回数（実際の待ち時間は、1回の待ち時間×待ち回数[msec]
const  _MYTIME_TYPE _timeout = 1000;				// 1回の待ち時間

const char *chMethod[sizeof(CHttpCom::HttpMethod)] = {"GET", "POST"};
const char *chVersion[CHttpMsg::HTTP_VER_NUM] = {"1.0", "1.1"};

#if 0
/** HTTP Replyを受信する
 * @return HTTPCOM_RETURN_OK 成功
 * @return SOCK_RECV_TIMEOUT タイムアウト
 */
int CHttpCom::mHttpRecv(string & dat)
{
	int ret = HTTPCOM_RETURN_OK;
	char buf[HTTPCOM_PRIV_RECV_UNIT];
	ssize_t szRecv;
	bool rc;
	short err = 0;
	DWORD count = 0;
	stringstream ss;

	ss.str("");
//	dat.clear();

	do{
		switch(this->m_proto)
		{
		case CHttpCom::HTTP:
			rc = this->m_pSock->Recv(buf, HTTPCOM_PRIV_RECV_UNIT-1, 0, &szRecv, &_com_to);
			if(rc == false)	err = this->m_pSock->GetLastError();
			break;

		case CHttpCom::HTTPS:
			rc = this->m_pSsl->Recv(buf, HTTPCOM_PRIV_RECV_UNIT-1, 0, &szRecv, &_com_to);
			if(rc == false)	err = this->m_pSsl->GetLastError();
			break;

		default:
			break;
		}

#ifdef _DEBUG
		if(count % 10 == 0)
		{
			MyOutputDebugString(_T("count=%d, Receive rc=%d, szRecv = %d, err = 0x%X\n"), count, rc, szRecv, err);
		}
#endif

		if(rc == true)
		{
			if(szRecv != -1)
			{
				// 正常受信
				buf[szRecv] = '\0';
				ss << string(buf);
			}
			else
			{
				// 切断
				//ss << "\0";
			}
		}
		else
		{
			if(err == SOCK_RECV_TIMEOUT)
			{
				if(++count != _timeout)
				{
					rc = true;
					szRecv = 1;
					Sleep(retry_time);
				}
			}
		}
	}while((rc == true) && (szRecv != -1));

#ifdef _DEBUG
	size_t len = ss.str().length();
	if(len != 0)
	{
		MyOutputDebugString(_T("Received(len=%d): [%*.s]\n"), len, 192, ss.str().c_str());
	}
#endif

	if(rc == false)
	{
		ret = err;
	}
	else
	{
		dat = ss.str();
	}
	
	return ret;
}
#endif

/** デフォルト値の初期化
 *
 */
void CHttpCom::mSetDefaultOption(void)
{
	this->m_fCookieCont = true;
//	this->m_fSendHost = false;
//	this->m_httpVer.clear();
}

string CHttpCom::mGetRequestString(const std::string *page, CHttpCom::HttpMethod method)
{
	stringstream ss;
	ss.str("");

	// METHOD
	ss << string(chMethod[method]) << " " << *page << " HTTP/" << chVersion[this->m_httpVer] << "\r\n";

	return ss.str();
}

void CHttpCom::mInit(void)
{
	this->m_cSHeader = new string[CHttpCom::HEADER_PRESET_NUM];

	// String Table
	this->m_cSHeader[CHttpCom::HEADER_CONTENT_TYPE] = string("Content-type");
	this->m_cSHeader[CHttpCom::HEADER_HOST_NAME] = string("Host");
}

/** 1行の受信
 * @return HTTPCOM_RETURN_OK OK
 * @return 通信のエラーコード（Sock.h参照）
 */
int CHttpCom::mRecvLine(std::string &line)
{
	int ret = HTTPCOM_RETURN_OK;
	stringstream ss;
	char buf, lch = '\0';
	
	ss.str("");

	do{
		ret = this->mRecvByte(buf);

		ss << buf;

		// "\r\n"で終了
		if((lch == '\r') && (buf == '\n'))	break;
		lch = buf;

	}while(ret == HTTPCOM_RETURN_OK);

	if(ret == HTTPCOM_RETURN_OK)
	{
		line = ss.str();
	}

	return ret;
}

/** 
 * @return HTTPCOM_RETURN_OK OK
 */
int CHttpCom::mRecvBody(const size_t size, std::string &body)
{
	size_t loop;
	char buf;
	stringstream ss;
	int ret;

	ss.str("");
		
	for(loop = 0; loop < size; ++loop)
	{
		ret = this->mRecvByte(buf);
		if(ret != HTTPCOM_RETURN_OK)	break;
		ss << buf;
	}

	body = ss.str();

	return ret;
}

/**
 * @return HTTPCOM_RETURN_OK OK
 * @return HTTPCOM_NOT_CONNECTED 切断
 * @retunr others ソケットエラー
 */
int CHttpCom::mRecvByte(char & dat)
{
	int ret = HTTPCOM_RETURN_OK;
	unsigned int cnt = 0;
	bool rc;
	char buf;
	ssize_t szRecv;
	short err;

	while(true)
	{
		switch(this->m_proto)
		{
		case CHttpCom::HTTP:
			rc = this->m_pSock->Recv(&buf, 1, 0, &szRecv, &_com_to);
			if(rc == false)	err = this->m_pSock->GetLastError();
			break;
#ifdef HAVE_SSL
		case CHttpCom::HTTPS:
			rc = this->m_pSsl->Recv(&buf, 1, 0, &szRecv, &_com_to);
			if(rc == false)	err = this->m_pSsl->GetLastError();
			break;
#endif
		}

		if(rc == true)
		{
			if(szRecv == -1)
			{						// 切断
				ret = HTTPCOM_NOT_CONNECTED;
			}
			else
			{						// 受信成功
				dat = buf;
			}
			break;
		}
		else
		{
			if(err == SOCK_RECV_TIMEOUT)
			{						// データが到達していない
				cnt++;
				if(cnt >= retry_time)
				{
					break;
				}
				rc = true;
				szRecv = 0;
				SLEEP(_timeout);		// Sleepする
			}
			else
			{
				break;
			}
		}
	}

	return ret;
}
