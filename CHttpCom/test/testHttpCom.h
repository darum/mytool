#pragma once

#include "gtest/gtest.h"

#include "HttpCom.h"

class CHttpComTest : public ::testing::Test
{
public:
	CHttpComTest(void);
	~CHttpComTest(void);

	void SetUp();
	void TearDown();

protected:
	void Common_GET_Test(void);

	CHttpCom *pHttp;

};
