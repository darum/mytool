#include "testHttpCom.h"

CHttpComTest::CHttpComTest(void)
{
}

CHttpComTest::~CHttpComTest(void)
{
}

void CHttpComTest::SetUp()
{
	this->pHttp = new CHttpCom(CHttpMsg::HTTP_VER_1_1);
}

void CHttpComTest::TearDown()
{
	delete(this->pHttp);
}

// Test Method
TEST_F(CHttpComTest, test01_HTTP_GET)
{
	int rc;

//	string server("192.168.1.100");
	string server("darum.dip.jp");	
	rc = this->pHttp->ConnectServer(&server, HttpCom_usHttpPort, CHttpCom::HTTP);
	ASSERT_EQ(HTTPCOM_RETURN_OK, rc);

	Common_GET_Test();

	rc = this->pHttp->Close();
	ASSERT_EQ(HTTPCOM_RETURN_OK, rc);
}

TEST_F(CHttpComTest, test02_HTTPS_GET)
{
//	string server("192.168.1.100");
	string server("darum.dip.jp");	
	ASSERT_EQ(HTTPCOM_RETURN_OK, this->pHttp->ConnectServer(&server, HttpCom_usHttpsPort, CHttpCom::HTTPS));

	Common_GET_Test();

	ASSERT_EQ(HTTPCOM_RETURN_OK, this->pHttp->Close());
}


void CHttpComTest::Common_GET_Test()
{
	int rc;
	string buff;

	ASSERT_EQ(HTTPCOM_RETURN_OK, this->pHttp->DumpRequestMsg(&buff));
	ASSERT_EQ((size_t)0, buff.length());

	string host("192.168.1.100");
	ASSERT_EQ(HTTPCOM_RETURN_OK, this->pHttp->SetHttpHeader(CHttpCom::HEADER_HOST_NAME, &host));
#if 0
	string dmyBody("dummy");
	ASSERT_EQ(HTTPCOM_RETURN_OK, this->pHttp->SetBody(&dmyBody));
#endif
	string page("/darum/pub/test.txt");
	ASSERT_EQ(HTTPCOM_RETURN_OK, this->pHttp->SendRequest(&page));
	ASSERT_EQ(HTTPCOM_RETURN_OK, this->pHttp->DumpRequestMsg(&buff));
	unsigned short code;
	rc = this->pHttp->GetResponse(&code);
	ASSERT_EQ(HTTPCOM_RETURN_OK, rc);
	ASSERT_EQ(HTTPCOM_RESP_OK, code);

	string strLen;
	rc = this->pHttp->GetHttpHeader(CHttpCom::HEADER_CONTENT_LEN, &strLen);
	ASSERT_EQ(HTTPCOM_RETURN_OK, rc);
	rc = this->pHttp->GetResBody(&buff);
	ASSERT_EQ(HTTPCOM_RETURN_OK, rc);
	ASSERT_EQ(buff.size(), (size_t)atoi(strLen.c_str()));
}

TEST_F(CHttpComTest, test03_ParseUrl)
{
	string server, page;
	unsigned short port;
	CHttpCom::Protocol proto;

	string url("http://www.yahoo.co.jp/");
	ASSERT_EQ(HTTPCOM_RETURN_OK, CHttpCom::ParseUrlString(&url, &proto, &server, &port, &page));
	ASSERT_EQ(CHttpCom::HTTP, proto);
	ASSERT_EQ(string("www.yahoo.co.jp"), server);
	ASSERT_EQ((unsigned short)0, port);
	ASSERT_EQ(string("/"), page);

	url = string("https://darum.dip.jp/");
	ASSERT_EQ(HTTPCOM_RETURN_OK, CHttpCom::ParseUrlString(&url, &proto, &server, &port, &page));
	ASSERT_EQ(CHttpCom::HTTPS, proto);

	url = string("http://darum.dip.jp:80/");
	ASSERT_EQ(HTTPCOM_RETURN_OK, CHttpCom::ParseUrlString(&url, &proto, &server, &port, &page));
	ASSERT_EQ(CHttpCom::HTTP, proto);
	ASSERT_EQ(string("darum.dip.jp"), server);
	ASSERT_EQ((unsigned short)80, port);
	ASSERT_EQ(string("/"), page);

	url = string("http://darum.dip.jp:80");
	ASSERT_EQ(HTTPCOM_RETURN_OK, CHttpCom::ParseUrlString(&url, &proto, &server, &port, &page));
	ASSERT_EQ(CHttpCom::HTTP, proto);
	ASSERT_EQ(string("darum.dip.jp"), server);
	ASSERT_EQ((unsigned short)80, port);
	ASSERT_EQ(string("/"), page);

	url = string("http://darum.dip.jp");
	ASSERT_EQ(HTTPCOM_RETURN_OK, CHttpCom::ParseUrlString(&url, &proto, &server, &port, &page));
	ASSERT_EQ(CHttpCom::HTTP, proto);
	ASSERT_EQ(string("darum.dip.jp"), server);
	ASSERT_EQ((unsigned short)0, port);
	ASSERT_EQ(string("/"), page);
}
