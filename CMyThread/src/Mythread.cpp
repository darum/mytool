#include "stdafx.h"		// DLL
#ifdef _WIN32
#pragma warning(disable : 4819) 
#endif

#include "stdlib.h"
#include "mythread.h"

using namespace mythread;

/** コンストラクタ
 *
 */
thread::thread()
{
  m_threadid = 0;
  m_run = false;
}

/** デストラクタ
 *
 */
thread::~thread()
{
}

/** スレッド開始エントリポイント
 * run()を呼び出します。
 * @param arg 実行するクラス
 */
THREAD_RET thread::start_routine(void* arg)
{
  if(arg == 0)
    return 0;
  thread* p = (thread*)arg;
  return p->run();
}

/** スレッドの開始
 * @return 0 OK
 * @return !0 エラーコード（スレッド生成失敗）
 */
int thread::start()
{
#ifdef _WIN32
	uintptr_t rc;
	rc = _beginthreadex( NULL, 0, (unsigned int (_stdcall *)(void *))thread::start_routine, this, 0, (unsigned *)&this->m_threadid );
	if( rc == 0 )
	{
		// Error
		return errno;
	}
	else
	{
		::Sleep(0);
		return 0;
	}
#else
	return::pthread_create(&m_threadid, 0, (void* (*)(void *))thread::start_routine, this);
#endif
}

/** スレッドの終了待ち
 * @param [out] ret スレッドの終了コード。NULLで値を取得しない。
 * @param [out] wait スレッド終了待ち時間。THREAD_WAIT_INFINITEで、無限待ち
 * @return 0 OK（スレッドは終了した）
 * @return 1 タイムアウト
 * @return <0 エラーコード
 */
int thread::join(THREAD_RET *ret, int wait)
{
	int retVal = 0;
#ifdef _WIN32
	DWORD rc;
	DWORD _wait;
	HANDLE hThread = ::OpenThread(THREAD_ALL_ACCESS, FALSE, this->m_threadid);
	_wait = (DWORD)wait;
	rc = WaitForSingleObject( hThread, _wait );
	if( rc == WAIT_FAILED )
	{
		retVal = (-(int)GetLastError());
	}
	else if(rc == WAIT_TIMEOUT)
	{
		retVal = 1;
	}
	else
	{
		BOOL b_rc;
		if(ret != NULL)
			b_rc = ::GetExitCodeThread(hThread, ret);
		retVal = 0;
	}
	::CloseHandle(hThread);
	return retVal;
#else
	int rc;
	THREAD_RET t_ret;
	void *v_ret = (void*)&t_ret;
	if((rc = pthread_join(m_threadid, &v_ret)) != 0)
	{
		retVal = -rc;
	}
	else
	{
	  if(v_ret == PTHREAD_CANCELED)
	    *ret = 0;
	  else
	    *ret = t_ret;
	}
	return retVal;
#endif
}

/** スレッドの終了
 * @note 実行中でないスレッドを停止しようとすると動作は保証されない
 * @note UINXでは、待ち時間は無効（無限待ち）
 * @param wait 待ち時間(0で無限)
 * @return 0 スレッドは終了
 * @return !0 終了エラー
 */
int thread::stop(THREAD_RET * ret, int wait)
{
	m_run = false;
	//	pthread_cancel(m_threadid);
	return join(ret, wait);
}

/** スレッドのメイン
 * スレッドの実装をこの関数に記述します。
 */
THREAD_RET thread::run()
{
  return 0;
}

#ifdef _WIN32
/** スレッドの実行状態取得
 * @note only Win32
 */
bool thread::get_run_state()
{
	DWORD rc;
	HANDLE hThread = ::OpenThread(THREAD_ALL_ACCESS, FALSE, this->m_threadid);
	DWORD rc2 = ::GetLastError();
	::GetExitCodeThread(hThread, &rc);
	::CloseHandle(hThread);
	if(rc == STILL_ACTIVE)
		return true;
	else
		return false;
}
#endif
