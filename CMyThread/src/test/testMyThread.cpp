#include "stdafx.h"
#include "testMyThread.h"

#ifdef _WIN32
#define SLEEP(a) Sleep((a))
#else
#define SLEEP(a) usleep((a))
#endif

CMyThreadTest::CMyThreadTest()
{

}

CMyThreadTest::~CMyThreadTest()
{
	
}

void CMyThreadTest::SetUp()
{

}

void CMyThreadTest::TearDown()
{

}

TEST_F(CMyThreadTest, AllTest)
{
#ifdef _WIN32
	bool state;
#endif
	int rc;
	THREAD_RET exit_code = 1;

	this->start();
#ifdef _WIN32
	SLEEP(0);
	state = this->get_run_state();
	ASSERT_EQ(true, state);

	rc = this->join( NULL, 200 );
	ASSERT_EQ(1, rc);		// timeout
#else
	SLEEP(1);
#endif

	rc = this->stop( &exit_code, THREAD_WAIT_INFNITE );		// 終了するまで待つ
	ASSERT_EQ(0, rc);
#ifdef _WIN32
	ASSERT_EQ((THREAD_RET)0, exit_code);
	state = this->get_run_state();
	ASSERT_EQ(false, state);
#endif
}

// テスト用 スレッドメイン
THREAD_RET CMyThreadTest::run(void)
{
	this->m_run = true;

	while(m_run)
	{
		SLEEP(0);
	}

	return 0;
}

