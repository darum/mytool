#include "stdafx.h"
#include "mythread.h"

class CMyThreadTest : public ::testing::Test, public mythread::thread
{
public:
	CMyThreadTest();
	~CMyThreadTest();

	void SetUp();
	void TearDown();

protected:
	THREAD_RET run();
};

