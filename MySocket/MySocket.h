// MySocket.h : MySocket.DLL のメイン ヘッダー ファイル
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CMySocketApp
// このクラスの実装に関しては MySocket.cpp を参照してください。
//

class CMySocketApp : public CWinApp
{
public:
	CMySocketApp();

// オーバーライド
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
