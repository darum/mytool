#!/bin/sh
RM='/bin/rm -f'
TARGET='CCalendar CCfgLoad CLog CMyThread CSock CSslSock'
TMP1=".tmp1.$$"

TOP=`pwd`

#echo $TARGET
for i in ${TARGET}
do
	echo "=== Test for ${i} ==="
	cd ${i}/src/
	./test1 > $TMP1 2>&1
	LL=`tail -1 $TMP1`
	echo ${LL} | grep ^OK > /dev/null
	if [ $? -eq 0 ]; then
	    echo "${i}: ${LL}"
	else
	    cat ${TMP1}
	    echo
	fi
	${RM} ${TMP1}
	cd ${TOP}
done

TARGET='CHttpCom'
for i in ${TARGET}
do
    echo "=== Test for ${i} ==="
    cd ${i}
    ./test1 > ${TMP1} 2>&1
    LL=`tail -1 ${TMP1}`
    echo ${LL} | grep ^OK > /dev/null
    if [ $? -eq 0 ]; then
	echo "${i}: ${LL}"
    else
	cat ${TMP1}
    fi
    ${RM} ${TMP1}
    cd ${TOP}
done

echo
echo "Test was finished."